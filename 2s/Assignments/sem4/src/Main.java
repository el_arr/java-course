import entities.Compiler;
import entities.FileManager;
import entities.checking.Reviewer;

public class Main {

    public static void main(String[] args) {
        String preliminary = FileManager.read();
        if (Reviewer.review(preliminary)) {
            FileManager.write(Compiler.compile(preliminary));
        }
    }

}