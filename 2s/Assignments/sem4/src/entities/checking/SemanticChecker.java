package entities.checking;

import javafx.util.Pair;

import java.util.regex.Pattern;

class SemanticChecker {

    static Pair<Boolean, String> checkOperator(String string) {
        String[] parts = string.split(":=");
        boolean res = false;
        try {
            res = match_id_1(parts) || match_id_2(parts);
        } catch (Exception e) {
            System.err.println(e.toString());
        }
        return (res) ? new Pair<>(true, "") : new Pair<>(false, "Ошибка в присвоении значений");
    }

    private static boolean match_id_1(String[] parts) {
        return checkPattern("[A-Z]{2}", parts[0]) &&
                parts[1].length() == 8 && checkPattern("[0-1]{8}", parts[1]);
    }

    private static boolean match_id_2(String[] parts) {
        return checkPattern("[A-Z][1-9]", parts[0]) &&
                checkSign("" + parts[1].charAt(0)) &&
                (checkPattern("[0-1]{8}", parts[1].substring(1)) ||
                        checkPattern("[A-Z]{2}", parts[1].substring(1)));
    }

    private static boolean checkPattern(String pattern, String s) {
        return Pattern.compile(pattern).matcher(s).matches();
    }

    private static boolean checkSign(String s) {
        boolean state = false;
        if (s.equals("◊") || s.equals("%") || s.equals("!")) {
            state = true;
        }
        return state;
    }
}