package entities.checking;

import javafx.util.Pair;

class SyntaxLexisChecker {

    private static State state = State.NULL;

    static Pair<Boolean, String> checkOperator(String in) {
        /* Метод состояний [автомат]:
         * 0 - начальное
         * 1 - буква, проверка следующего символа, буква или цифра -> 0
         * знак -> 0
         * ';' | ' ' -> 0
         * 2 - ':' ->('=')-> 0
         * 3 - цифра, проверка следующих семи символов -> 0
         * 4 - финальное
         * 5 - мусорка
         */
        char c;
        int i = 0;
        boolean noWarnings = true;
        while (i < in.length() && noWarnings) {
            c = in.charAt(i);
            switch (state) {
                case NULL:
                    if (c >= 'A' && c <= 'Z') {
                        state = State.FIRST;
                    } else if (c == '!' || c == '%' || c == '◊') {
                        state = State.NULL;
                    } else if (c == ';') {
                        state = State.NULL;
                    } else if (c == ':') {
                        state = State.SECOND;
                    } else if (c == '0' || c == '1') {
                        state = State.THIRD;
                    } else {
                        setFifthState(i);
                    }
                    break;
                case FIRST:
                    if (c >= 'A' && c <= 'Z') {
                        state = State.NULL;
                    } else if (c >= '0' && c <= '9') {
                        state = State.NULL;
                    } else {
                        setFifthState(i);
                    }
                    break;
                case SECOND:
                    if (c == '=') {
                        state = State.NULL;
                    } else {
                        setFifthState(i);
                    }
                    break;
                case THIRD:
                    identify_code(in.substring(i, i + 7), i);
                    i += 7;
                    break;
            }
            if (state == State.FIFTH) {
                noWarnings = false;
            }
            i++;
        }
        return new Pair<>(noWarnings, (noWarnings) ? "" : state.toString());
    }

    private static void setFifthState(int i) {
        String s = state.getWarning();
        state = State.FIFTH;
        state.setWarning(s);
        state.set_symbol_num(i);
    }

    private static void identify_code(String s, int pos) {
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) != '1' && s.charAt(i) != '0') {
                setFifthState(pos);
            }
        }
        state = State.NULL;
    }

    private enum State {
        NULL(0, "Встречен недопустимый символ на"),
        FIRST(0, "Недопустимый символ в названии идентификатора на"),
        SECOND(0, "Встречен недопустимый символ после ':'(ожидалось '=') на"),
        THIRD(0, "Встречен недопустимый символ в записи числа (ожидалось '0'|'1') после"),
        FOURTH(0, "Символ окончания оператора в недопустимом месте на"),
        FIFTH(0, "");

        int symbol_num;
        String warning;

        State(int symbol_num, String warning) {
            this.symbol_num = symbol_num;
            this.warning = warning + " позиции ";
        }

        public void set_symbol_num(int n) {
            this.symbol_num = n;
        }

        public String getWarning() {
            return warning;
        }

        public void setWarning(String warning) {
            this.warning = warning;
        }

        @Override
        public String toString() {
            return this.warning + this.symbol_num;
        }
    }

}
