package ru.kpfu.itis.el_arr.stackoverflowsort;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class ReadingAndWriting {

    private static String[] fileToStringArray(String name) {
        File f = new File("res\\" + name + ".txt");
        String[] s;
        try {
            BufferedReader in = new BufferedReader(new FileReader(f));
            s = in.readLine().split(", ");
            in.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return s;
    }

    static ArrayList<Integer> readToArrayList(String name) {
        String[] s = fileToStringArray(name);
        ArrayList<Integer> res = new ArrayList<>(s.length);
        for (int i = 0; i < s.length; i++) {
            res.add(Integer.valueOf(s[i]));
        }
        return res;
    }

    static LinkedList<Integer> readToLinkedList(String name) {
        String[] s = fileToStringArray(name);
        LinkedList<Integer> res = new LinkedList<>();
        for (int i = 0; i < s.length; i++) {
            res.add(Integer.valueOf(s[i]));
        }
        return res;
    }

    static int[] readToArray(String name) {
        int[] res;
        String[] s = fileToStringArray(name);
        res = new int[s.length];
        for (int i = 0; i < s.length; i++) {
            res[i] = Integer.valueOf(s[i]);
        }
        return res;
    }

    static void generate() {
        File f = new File("res\\array.txt");
        Random rn = new Random();
        final int MAX = 1000;
        final int MIN = 100;
        int n = MAX - MIN + 1;
        int i = Math.abs(rn.nextInt() % n);
        n = MIN + i;
        int[] a = new int[n];
        for (i = 0; i < n; i++) {
            a[i] = rn.nextInt() % (MAX + 1);
        }
        String s = Arrays.toString(a);
        s = s.substring(1, s.length() - 1);
        write(s, f);
        System.out.println("Данные сгенерированы. Результат в *res/array.txt*");
    }

    static void codeToFile(String string, String type) {
        File f = new File("src\\ru\\kpfu\\itis\\el_arr\\stackoverflowsort\\Run.java");
        Pattern p = Pattern.compile("[a-z]([A-z]?[0-9]?)*\\(");
        Matcher m = p.matcher(string);
        String s = "";
        if (m.find()) {
            s = m.group().substring(0, m.group().length() - 1);
        }
        String mainContent = "package ru.kpfu.itis.el_arr.stackoverflowsort;\n" +
                "\n" +
                "import java.util.ArrayList;\n" +
                "import java.util.Arrays;\n" +
                "import java.util.LinkedList;\n" +
                "\n" +
                "public class Run {\n" + "\n" +
                "    public static void main(String[] args) {\n" +
                defineType(type, s) + '\n' + string + '\n' + '}';
        write(mainContent, f);
    }

    static void writeResult(String s) {
        File f = new File("res\\result.txt");
        write(s.substring(1, s.length() - 1), f);
    }

    private static void write(String s, File f) {
        try {
            if (!f.exists())
                f.createNewFile();
            FileWriter writer = new FileWriter(f, false);
            writer.write(s);
            writer.close();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    private static String defineType(String type, String method) {
        String dataType;
        String write;
        if (type.equals("ArrayList") || type.equals("LinkedList")) {
            dataType = type + "<Integer>";
            write = "array.toString()";
        } else {
            dataType = "int[]";
            type = "Array";
            write = type + "s.toString(array)";
        }
        return "        " + dataType + " array = ReadingAndWriting.readTo" + type + "(\"array\");\n" +
                "        array = " + method + "(array);\n" +
                "        ReadingAndWriting.writeResult(" + write + ");\n" +
                "    }\n";
    }
}
