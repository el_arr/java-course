import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GraphicPic {
    private Segment head;

    public GraphicPic() {
    }

    public GraphicPic(String filename) {
        File f = new File("res\\" + filename);
        Segment saved = null;
        int[] arr = new int[4];
        String s;
        Pattern p = Pattern.compile("([0-9]+|([1-9][0-9]+))");
        try {
            BufferedReader in = new BufferedReader(new FileReader(f));
            try {
                while ((s = in.readLine()) != null) {
                    Matcher m = p.matcher(s);
                    int i = 0;
                    while (m.find()) {
                        arr[i] = Integer.valueOf(m.group());
                        i++;
                    }
                    head = new Segment(arr[0], arr[1], arr[2], arr[3], saved);
                    saved = head;
                }
            } finally {
                in.close();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void show() {
        Segment t = head;
        while (t != null) {
            System.out.println(t);
            t = t.getNext();
        }
    }

    public void insert(Segment f) {
        boolean notFound = true;
        Segment t = head;
        while (t != null) {
            if (t.equals(f) && notFound) {
                notFound = false;
            }
            t = t.getNext();
        }
        if (notFound) {
            f.setNext(head);
            head = f;
        }
    }

    public GraphicPic angleList() {
        GraphicPic res = new GraphicPic();
        Segment temp = null;
        Segment t = head;
        Segment saved;
        while (t != null) {
            double angle = (t.getE().getY() - t.getB().getY()) / (t.getE().getX() - t.getB().getX());
            if (Math.atan(angle) == 45 || Math.atan(angle) == 30) {
                saved = temp;
                temp = new Segment(t);
                temp.setNext(saved);
                res.setHead(temp);
            }
            t = t.getNext();
        }
        return res;
    }

    public GraphicPic lengthList(int a, int b) {
        GraphicPic res = new GraphicPic();
        Segment temp = null;
        Segment t = head;
        Segment saved;
        while (t != null) {
            if (t.getLength() <= b && t.getLength() >= a) {
                saved = temp;
                temp = new Segment(t);
                temp.setNext(saved);
                res.setHead(temp);
            }
            t = t.getNext();
        }
        return res;
    }

    public void sort() {
        ArrayList<Segment> a = new ArrayList<>();
        Segment t = head;
        while (t != null) {
            a.add(t);
            t = t.getNext();
        }
        a.sort((Segment o1, Segment o2) -> Double.compare(o1.getLength(), o2.getLength()));
        Segment n = a.get(0);
        this.head = n;
        for (int i = 1; i < a.size(); i++) {
            n.setNext(a.get(i));
            n = a.get(i);
        }

    }


    public void setHead(Segment head) {
        this.head = head;
    }

}