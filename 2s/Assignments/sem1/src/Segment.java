public class Segment {
    private Point b;
    private Point e;
    private Segment next;
    private double length;

    public Segment(Segment s) {
        this(s.b.x, s.b.y, s.e.x, s.e.y, null);
    }

    public Segment(Integer bx, Integer by, Integer ex, Integer ey, Segment next) {
        b = new Point(bx, by);
        e = new Point(ex, ey);
        this.next = next;
        double f = ey - by;
        double s = ex - bx;
        length = Math.sqrt(f * f + s * s);
    }

    public String toString() {
        return "(" + b.x + " ; " + b.y + " ; " + e.x + " ; " + e.y + ")";
    }

    public boolean equals(Segment s) {
        //Это просто совпадение, случайно выбранные названия переменных
        if (this.b.x == s.b.x && this.b.y == s.b.y && this.e.x == s.e.x && this.e.y == s.e.y) {
            return true;
        } else
            return false;
    }

    public Point getB() {
        return b;
    }

    public Point getE() {
        return e;
    }

    public Segment getNext() {
        return next;
    }

    public void setNext(Segment next) {
        this.next = next;
    }

    public double getLength() {
        return length;
    }

    public class Point {
        private int x;
        private int y;

        public Point() {
            x = 0;
            y = 0;
        }

        public Point(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public int getX() {
            return x;
        }

        public int getY() {
            return y;
        }
    }
}