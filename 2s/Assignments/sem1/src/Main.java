public class Main {

    public static void main(String[] args) {
        GraphicPic gp = new GraphicPic("lines.txt");
        gp.show();
        System.out.println();
        GraphicPic ngp = gp.lengthList(0, 10);
        ngp.show();
        System.out.println();
        gp.sort();
        gp.show();
    }

}
