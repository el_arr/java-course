public class Elem {
    private Integer[] value;
    private Elem next;

    public Elem() {
    }

    public Elem(Integer[] value, Elem next) {
        this.value = value;
        this.next = next;
    }

    public Integer[] getValue() {
        return value;
    }

    public void setValue(Integer[] value) {
        this.value = value;
    }

    public Elem getNext() {
        return next;
    }

    public void setNext(Elem next) {
        this.next = next;
    }

    public double average() {
        int sum = 0;
        int count = 0;
        for (Integer i : this.value) {
            sum += i;
            count++;
        }
        return sum / count;
    }

    public boolean allPositive() {
        for (int i = 0; i < this.value.length; i++) {
            if (value[i] < 0) {
                return false;
            }
        }
        return true;
    }

    public Integer[] copy() {
        Integer[] res = new Integer[this.value.length];
        for (int i = 0; i < this.value.length; i++) {
            res[i] = this.value[i];
        }
        return res;
    }
}