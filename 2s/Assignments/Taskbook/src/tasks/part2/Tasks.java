package tasks.part2;

import tasks.init.Init;
import tasks.part_1.utility.Elem;
import tasks.part_1.utility.MyLinkedList;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * @author Eldar Mingachev
 *         KPFU ITIS 11-601
 */
public class Tasks {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        //MyLinkedList<Integer> list = Init.myLinkedListInit(in);
        //tasks_014_015(list, new int[]{2}, in);
        //tasks_014_015(list, new int[]{2, 3}, in);
        //task016(list, in);
        //task017(list);
        //task018(list, in);
        //task019(list);
        //task020(list);
        //task021(list, in);
        //task023(list, in);
        //task024(list);
        //task025(list);
    }

    private static void tasks_014_015(MyLinkedList<Integer> list, int[] divBy, Scanner in) {
        Elem<Integer> temp = list.getHead();
        while (temp != null) {
            temp = insertAfterDivBy(temp, divBy, in);
            temp = temp.getNext();
        }
        temp = list.getHead();
        System.out.println(list);
    }

    private static Elem<Integer> insertAfterDivBy(Elem<Integer> temp, int[] divBy, Scanner in) {
        Elem<Integer> insert;
        if (checkDivBy(temp.getValue(), divBy)) {
            System.out.println("Введите вставляемое значение");
            insert = new Elem<>(in.nextInt(), temp.getNext());
            temp.setNext(insert);
            temp = insert;
        }
        return temp;
    }

    private static boolean checkDivBy(int e, int[] arr) {
        for (int i : arr) {
            if (e % i == 0) {
                return true;
            }
        }
        return false;
    }

    private static void task016(MyLinkedList<Integer> list, Scanner in) {
        tasks_014_015(list, new int[]{2}, in);
    }

    private static void task017(MyLinkedList<Integer> list) {
        ArrayList<Integer> array = primeNumbers(list);

        Elem<Integer> temp = list.getHead(),
                prev = null,
                next, pre, after;

        while (temp != null) {
            next = temp.getNext();
            if (checkIfSimple(temp.getValue(), array)) {
                pre = new Elem<>(getFirstDigit(temp.getValue()), list.getHead());
                after = new Elem<>(temp.getValue() % 10, next);
                if (prev == null) {
                    list.setHead(pre);
                    temp.setNext(after);
                } else {
                    prev.setNext(pre);
                    pre.setNext(temp);
                    temp.setNext(after);
                    after.setNext(next);
                }
                prev = after;
            } else {
                prev = temp;
            }
            temp = next;
        }
        System.out.println(list);
    }

    private static ArrayList<Integer> primeNumbers(MyLinkedList<Integer> list) {
        int max = list.maxValue();
        ArrayList<Integer> array = new ArrayList<>();

        array.add(2);
        for (int i = 3; i <= max; i++) {
            if (isSimple(i, array)) {
                array.add(i);
            }
        }
        array.add(0, 1);

        return array;
    }

    private static boolean isSimple(int value, ArrayList<Integer> array) {
        return array
                .stream()
                .noneMatch(o -> value % o == 0);
    }

    private static boolean checkIfSimple(int value, ArrayList<Integer> array) {
        return array.stream().anyMatch(o -> o == value);
    }

    private static int getFirstDigit(int value) {
        while (value / 10 != 0) {
            value = value / 10;
        }
        return value;
    }

    private static void task018(MyLinkedList<Integer> list, Scanner in) {
        int l = in.nextInt(),
                k, m;
        for (int i = 0; i < l; i++) {
            k = in.nextInt();
            m = in.nextInt();
            list.insert(k, m);
        }
        System.out.println(list);
    }

    private static void task019(MyLinkedList<Integer> list) {
        int length = list.length();
        for (int i = 1; i <= 3 && (length - i) >= 0; i++) {
            list.removeByIndex(length - i);
        }
        System.out.println(list);
    }

    private static void task020(MyLinkedList<Integer> list) {
        Elem<Integer> prev = list.getHead();
        Elem<Integer> temp = prev;
        while (temp != null) {
            if (temp.getValue() % 2 == 1) {
                list.removeByLinks(prev, temp);
            }
            prev = temp;
            temp = temp.getNext();
        }
        System.out.println(list);
    }

    private static void task021(MyLinkedList<Integer> list, Scanner in) {
        list.removeAllByValue(in.nextInt());
        System.out.println(list);
    }

    private static void task022(MyLinkedList<Integer> list1, MyLinkedList<Integer> list2) {
        MyLinkedList<Integer> new_list = new MyLinkedList<>();
        if (list1.getHead().getValue() > list2.getLast().getValue()) {
            list2.concat(list1);
            new_list = list2;
        } else if (list2.getHead().getValue() > list1.getLast().getValue()) {
            list1.concat(list2);
            new_list = list1;
        } else if (list1.getHead().getValue() > list2.getHead().getValue()) {
            Elem<Integer> head1 = list1.getHead(),
                    temp = list2.getHead();
            while (temp.getValue() < head1.getValue()) {
                new_list.add(temp.getValue());
            }
        }

    }

    private static void task023(MyLinkedList<Integer> list, Scanner in) {
        int n = in.nextInt();
        for (int i = 0; i < n; i++) {
            list.removeByIndex(in.nextInt());
        }
        System.out.println(list);
    }

    private static void task024(MyLinkedList<Integer> even_list) {
        MyLinkedList<Integer> odd_list = new MyLinkedList<>();
        Elem<Integer> temp = even_list.getHead();
        while (temp != null) {
            int value = temp.getValue();
            if (value % 2 == 1) {
                odd_list.add(value);
                even_list.removeByValue(value);
            }
            temp = temp.getNext();
        }
        System.out.println(even_list);
        System.out.println(odd_list);
    }

    private static void task025(MyLinkedList<Integer> list) {
        Elem<Integer> temp = list.getHead();
        String res = "";
        while (temp != null) {
            res += temp.getValue();
            list.removeByValue(temp.getValue());
            temp = temp.getNext();
        }
        list.add(toDecimal(res));
        System.out.println(list);
    }

    private static int toDecimal(String s) {
        int res = 0;
        int mult = 1;
        int pre = Integer.parseInt(s);
        for (int i = 0; i < s.length(); i++) {
            int elem = pre % 10;
            pre /= 10;
            elem *= mult;
            mult *= 2;
            res += elem;
        }
        return res;
    }
}
