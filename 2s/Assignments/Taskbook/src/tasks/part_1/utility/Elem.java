package tasks.part_1.utility;

public class Elem<T> {
    private T value;
    private Elem next;

    public Elem() {
    }

    public Elem(T value, Elem next) {
        this.value = value;
        this.next = next;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    public Elem<T> getNext() {
        return next;
    }

    public void setNext(Elem next) {
        this.next = next;
    }

    @Override
    public String toString() {
        return value + "";
    }
}