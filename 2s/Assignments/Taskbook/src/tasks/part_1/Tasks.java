package tasks.part_1;

import tasks.init.Init;
import tasks.part_1.utility.Elem;
import tasks.part_1.utility.MyLinkedList;

import java.util.LinkedList;
import java.util.Scanner;

/**
 * @author Eldar Mingachev
 *         KPFU ITIS 11-601
 */
public class Tasks {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        //LinkedList<Integer> list = Init.linkedListInit(in);
        MyLinkedList<Integer> myList = Init.myLinkedListInit(in);
        //task004(list);
        //task007(list);
        //task008(list);
        //task009test(myList);
        //task010(myList);
        //task011(myList);
        //task012test(myList, 2);
        //task013test(myList, 2);
    }

    private static void task004(LinkedList<Integer> list) {
        System.out.println(task004Null(list));
        System.out.println(task004A(list));
        System.out.println(task004B(list));
    }

    private static int task004Null(LinkedList<Integer> list) {
        int sum = 0;
        for (int i : list) {
            if (i % 2 == 0) {
                sum += i;
            }
        }
        return sum;
    }

    private static boolean task004A(LinkedList<Integer> list) {
        for (int i : list) {
            if (i == 0) {
                return true;
            }
        }
        return false;
    }

    private static int task004B(LinkedList<Integer> list) {
        int pos = 1;
        int res = 1;
        for (int i : list) {
            if (pos % 2 == 1) {
                res *= i;
            }
            pos++;
        }
        return res;
    }

    private static int task007(LinkedList<Integer> list) {
        return task004B(list);
    }

    private static void task008(MyLinkedList<Integer> list) {
        //учитывать ли первое и последнее значения?
        int min = 0, max = 0;
        Elem<Integer> prev = list.getHead();
        Elem<Integer> temp = prev.getNext();
        Elem<Integer> next = temp.getNext();
        while (temp.getNext() != null) {
            if (prev.getValue() < temp.getValue() && temp.getValue() > next.getValue()) {
                max++;
            } else if (prev.getValue() > temp.getValue() && temp.getValue() < next.getValue()) {
                min++;
            }
            prev = temp;
            temp = next;
            next = next.getNext();
        }
        System.out.println("Max: " + max + ". Min: " + min + ".");
    }

    private static void task009test(MyLinkedList<Integer> list) {
        list.reverse();
        System.out.println(list);
    }

    private static void task010(MyLinkedList<Integer> list) {
        Elem<Integer> head = list.getHead(),
                temp = head,
                temp1, temp2, prev;
        int n = 0;
        while (temp != null) {
            n++;
            temp = temp.getNext();
        }
        for (int i = 0; i < n; i++) {
            temp = head;
            prev = head;
            for (int j = 0; j < n; j++) {
                temp1 = temp.getNext();
                if (temp1 == null) {
                    continue;
                } else if (temp.getValue() > temp1.getValue()) {
                    temp2 = temp1.getNext();
                    if (j == 0) {
                        temp.setNext(temp2);
                        temp1.setNext(temp);
                        list.setHead(temp1);
                        temp = temp1;
                        temp1 = temp.getNext();
                    } else {
                        temp.setNext(temp2);
                        temp1.setNext(temp);
                        prev.setNext(temp1);
                        temp = temp1;
                        temp1 = temp.getNext();
                    }
                }
                prev = temp;
                temp = temp1;
            }
        }
        temp1 = head;
        while (temp != null) {
            n--;
            temp = temp.getNext();
            if (n == 1) {
                temp1 = temp;
            }
        }
        list.setLast(temp1);
        System.out.println(list);
    }

    private static void task011(MyLinkedList<Integer> list) {
        MyLinkedList<Integer> newList = new MyLinkedList<>();
        while (list.getHead() != null) {
            newList.add(findTempMin(list).getValue());
        }
        list = newList;
        System.out.println(list);
    }

    private static Elem<Integer> findTempMin(MyLinkedList<Integer> list) {
        Elem<Integer> temp = list.getHead(),
                min = temp;
        while (temp != null) {
            if (temp.getValue() < min.getValue()) {
                min = temp;
            }
            temp = temp.getNext();
        }
        list.removeByValue(min.getValue());
        return min;
    }

    private static void task012test(MyLinkedList<Integer> list, int k) {
        list.shiftToEnd(k);
        System.out.println(list);
    }

    private static void task013test(MyLinkedList<Integer> list, int k) {
        list.shiftToHead(k);
        System.out.println(list);
    }
}