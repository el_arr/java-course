package praxis.backTracking.task2;

import java.util.List;

public class SuperNode<T> {
    int label;
    T info;
    SuperNode next;
    List<SuperNode> jointNodes;
}


