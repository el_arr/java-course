package praxis.backTracking.task1;

import java.util.Arrays;

public class Solution {
    private int[] solution;
    private int count = 0;

    public Solution(int start, int size) {
        solution[0] = start;
        solution = new int[size];
    }

    public void add(int i) {
        solution[count] = i;
        count++;
    }

    public int getLength() {
        return count + 1;
    }

    public void remove() {
        count--;
    }

    public int[] getSolution() {
        return Arrays.copyOf(solution, count);
    }

    @Override
    public String toString() {
        return (Arrays.toString(solution)).substring(1, solution.length);
    }
}
