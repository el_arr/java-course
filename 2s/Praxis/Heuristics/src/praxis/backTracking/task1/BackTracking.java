package praxis.backTracking.task1;

public class BackTracking {
    private static final int SIZE = 5;
    private static final int LANG = 2;

    public static void main(String[] args) {
        for (int i = 0; i < LANG; i++) {
            backTracking(new Solution(i, SIZE), SIZE);
        }
    }

    private static void backTracking(Solution s, int n) {
        if (s.getLength() == n) {
            System.out.println(s + "[Good]");
        } else {
            for (int i = 0; i < LANG; i++) {
                s.add(i);
                if (check(s)) {
                    backTracking(s, n + 1);
                } else {
                    System.out.println(s + "[Warning]");
                }
            }
        }
    }

    private static boolean check(Solution s) {
        int count = 0;
        for (int i : s.getSolution()) {
            if (i == 1) {
                count++;
                if (count > 1) {
                    return false;
                }
            }
        }
        return true;
    }
}
