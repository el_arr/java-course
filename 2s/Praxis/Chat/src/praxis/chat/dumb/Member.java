package praxis.chat.dumb;

import java.io.*;

public class Member extends Thread {
    private String states = "state.txt";
    private String comm_file_1 = "comm_file_1.txt";
    private String comm_file_2 = "comm_file_2.txt";

    public static void main(String[] args) {
        (new Member()).start();
    }

    @Override
    public void run() {
        try {
            int x = readState();
            if (x == 0) {
                System.out.println("Chat created. Waiting for member to chat...");
                writeState(1);
                waiting();
                System.out.println("praxis.chat.dumb.Member found. Starting the chat");
            } else if (x == 1) {
                System.out.println("Joining existing chat.");
                writeState(2);
                BufferedReader br = new BufferedReader(
                        new InputStreamReader(
                                new FileInputStream(comm_file_1)));
                String input = br.readLine();
                while (input == null) {
                    br.close();
                    br = new BufferedReader(
                            new InputStreamReader(
                                    new FileInputStream(comm_file_1)));
                    input = br.readLine();
                }
                System.out.println("Guest:" + input);
                //flushing
                PrintWriter pw = new PrintWriter(comm_file_1);
                pw.close();

            } else if (x == 2) {
                System.out.println("OCCUPIED!");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            writeState(0);
        }
    }

    private void waiting() {
        int x = readState();
        try {
            while (x == 1) {
                Thread.sleep(1000);
                x = readState();
            }
        } catch (InterruptedException e) {
            System.err.println(e.getMessage());
        }
    }

    private void writeState(int b) {
        try {
            File f;
            if (!(f = (new File(states))).exists()) {
                f.createNewFile();
            }
            FileOutputStream fos = new FileOutputStream(states);
            fos.write(b);
            fos.close();
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }

    private int readState() {
        try {
            if (!(new File(states)).exists()) {
                writeState(0);
            }
            return (new FileInputStream(states)).read();
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
        return 404;
    }
}
