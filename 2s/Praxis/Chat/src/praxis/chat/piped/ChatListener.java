package praxis.chat.piped;

import javafx.scene.control.TextArea;

import java.io.BufferedReader;
import java.io.IOException;

public class ChatListener extends Thread {
    private TextArea textArea;
    private BufferedReader reader;

    public ChatListener()

    @Override
    public void run() {
        while (true) {
            try {
                System.out.println("W");
                String line = reader.readLine();
                textArea.setText(
                        textArea.getText() + line + "\n"
                );
            } catch (IOException e) {
                System.err.println(e.getMessage());
            }
        }
    }
}
