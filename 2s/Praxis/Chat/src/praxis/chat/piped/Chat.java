package praxis.chat.piped;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.stage.Stage;

import java.io.PipedInputStream;
import java.io.PipedOutputStream;

public class Chat extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        PipedOutputStream pos1 = new PipedOutputStream();
        PipedInputStream pis1 = new PipedOutputStream(pos1);
        PipedOutputStream pos2 = new PipedOutputStream();
        PipedInputStream pis2 = new PipedOutputStream(pos2);
    }
}
