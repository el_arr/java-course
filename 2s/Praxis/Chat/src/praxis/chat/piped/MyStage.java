package praxis.chat.piped;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.stage.Stage;

import java.io.*;

public class MyStage extends Stage {
    private BufferedReader reader;
    private PrintWriter writer;

    public MyStage(PipedInputStream pis, PipedOutputStream pos) {
        super();
        this.reader = new BufferedReader(new InputStreamReader(pis));
        this.writer = new PrintWriter(pos, true);
        initialize();
    }

    private void initialize() {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("Window.fxml"));

            ChatListener cl = new ChatListener(ta, reader);
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }
}
