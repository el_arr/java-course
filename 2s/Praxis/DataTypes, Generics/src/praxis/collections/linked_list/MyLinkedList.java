package praxis.collections.linked_list;

public class MyLinkedList<T> {
    private Elem head;
    private Elem last;

    public MyLinkedList(Elem head) {
        this.head = head;
        Elem p = head;
        while (p != null) {
            this.last = p;
            p = p.getNext();
        }
    }

    public MyLinkedList(T x) {
        head = new Elem<>(x, null);
        last = head;
    }

    public void add(T x) {
        Elem p = new Elem<>(x, null);
        last.setNext(p);
        last = p;
    }

    public void addHead(T x) {
        Elem p = new Elem<>(x, head);
        head = p;
    }

    public void insert(int i, T x) {
        Elem prev = head;
        Elem temp = head;
        for (int c = 0; c < i && temp != null; c++) {
            prev = temp;
            temp = temp.getNext();
        }
        Elem p = new Elem<>(x, temp);
        prev.setNext(p);
    }

    public void removeByIndex(int i) {
        Elem prev = head;
        Elem temp = head;
        for (int c = 0; c < i && temp != null; c++) {
            prev = temp;
            temp = temp.getNext();
        }
        try {
            prev.setNext(temp.getNext());
        } catch (NullPointerException e) {
            prev.setNext(null);
        }
    }

    public void removeByValue(T x) {
        Elem prev = head;
        Elem temp = head;
        boolean fl = false;
        while (temp != null && !fl) {
            if (temp.getValue().equals(x)) {
                fl = true;
            } else {
                prev = temp;
                temp = temp.getNext();
            }
        }
        if (fl) {
            prev.setNext(temp.getNext());
        } else
            System.out.println("Элемент не найден");
    }

    public void removeAfter(T x) {
        Elem temp = head;
        boolean fl = false;
        while (temp != null && !fl) {
            if (temp.getValue().equals(x)) {
                fl = true;
            } else {
                temp = temp.getNext();
            }
        }
        if (fl) {
            temp.setNext(temp.getNext().getNext());
        } else
            System.out.println("Элемент не найден");
    }

    public void removeHead() {
        head = head.getNext();
    }

    public void concat(MyLinkedList l) {
        this.last.setNext(l.head);
        Elem p = l.head;
        while (p != null) {
            this.last = p;
            p = p.getNext();
        }
    }

    public Elem getHead() {
        return head;
    }

    public Elem getLast() {
        return last;
    }
}
