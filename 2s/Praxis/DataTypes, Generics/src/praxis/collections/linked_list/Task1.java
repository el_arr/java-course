package praxis.collections.linked_list;

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Elem head = null;
        Elem p = new Elem();
        Scanner in = new Scanner(System.in);

        System.out.println("Введите размер списка");
        int n = in.nextInt();
        System.out.println("Введите n элементов");
        for (int i = 0; i < n; i++) {
            p = new Elem<>(in.nextInt(), head);
            head = p;
        }

        boolean fl = false;
        while (p != null && !fl) {
            if (((Elem<Integer>) p).getValue() < 0) {
                fl = true;
            }
            p = p.getNext();
        }
    }
}
