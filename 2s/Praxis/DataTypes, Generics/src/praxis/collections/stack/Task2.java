import praxis.collections.stack.LinkedCharStack;

public class Task2 {

    public static void main(String[] args) throws Exception {

        String s = "(1+2)";
        LinkedCharStack stack = new LinkedCharStack();
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            System.out.println(c);
            if (c == '(') {
                stack.push('(');
            } else if (c == ')') {
                if (stack.isEmpty()) {
                    throw new Exception("LoL");
                }
                stack.pop();
            }
        }
    }
}
