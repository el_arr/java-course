package praxis.collections.stack;

interface IMyStack<T> {

    T pop();

    void push(T x);

    boolean isEmpty();

}
