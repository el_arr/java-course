import praxis.collections.stack.LinkedCharStack;

public class Task1 {

    public static void main(String[] args) {
        char a[] = {'k', 'c', 'u', 'f'};
        LinkedCharStack stack = new LinkedCharStack();
        for (char c : a) {
            stack.push(c);
        }
        for (int i = 0; i < a.length; i++) {
            a[i] = stack.pop();
            System.out.print(a[i]);
        }
    }

}