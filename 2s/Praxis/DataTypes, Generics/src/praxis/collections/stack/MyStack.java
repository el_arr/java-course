package praxis.collections.stack;

public class MyStack<T> implements IMyStack<T> {

    private Elem<T> head;

    public void push(T x) {
            head = new Elem<>(x, head);
    }

    public T pop() {
        if (this.isEmpty()) {
            return null;
        }
        T c = head.getValue();
        head = head.getNext();
        return c;
    }

    public boolean isEmpty() {
        return head == null;
    }

}
