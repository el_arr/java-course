package praxis.collections.stack;

class Elem<T> {
    private T value;
    private Elem<T> next;

    public Elem(T value, Elem<T> next) {
        this.value = value;
        this.next = next;
    }

    public T getValue() {
        return value;
    }

    public Elem<T> getNext() {
        return next;
    }
}