package praxis.collections.trees;

public class BinaryTree<T> {
    private BinaryTreeNode<T> root;

    private static void makeTreeOnNode(BinaryTreeNode<Integer> node, int n) {
        int nLeft = n / 2;
        int nRight = n - 1 - nLeft;
        node.setValue(n);
        if (nLeft > 0) {
            node.setLeft(new BinaryTreeNode<>());
            makeTreeOnNode(node.getLeft(), nLeft);
        }
        if (nRight > 0) {
            node.setRight(new BinaryTreeNode<>());
            makeTreeOnNode(node.getRight(), nRight);
        }
    }

    public BinaryTree (T n) {
        root = new BinaryTreeNode<T>();
    }
}
