package praxis.collections.queue;

interface MyQueue<T> {

    void push(T o);

    T poll();

    boolean isEmpty();

}