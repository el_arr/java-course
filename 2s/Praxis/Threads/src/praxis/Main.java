package praxis;

import praxis.adder.Adder;

public class Main {

    public static void main(String[] args) {
        int[] array = {7, 5, 4, 1, 3, 6, 2, 9, 8};
        task01(array);
        task02(array);
    }

    private static void task01(int[] a) {
        (new Adder(a, 0, a.length / 2)).start();
        (new Adder(a, a.length / 2, a.length)).start();
        System.out.println(Adder.getCollector());
    }

    private static void task02(int[] a) {

    }

}


