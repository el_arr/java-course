package praxis.db.cars.repositories;

import praxis.db.cars.DataBaseConnection;
import praxis.db.cars.entities.CarMaker;

import java.util.List;
import java.util.stream.Collectors;

public class CarMakerRepository {

    private DataBaseConnection dbConnection;

    public CarMakerRepository(DataBaseConnection dbConnection) {
        this.dbConnection = dbConnection;
    }

    public List<CarMaker> getAll() {
        return dbConnection.getDBConnection().getAllCarMakers();
    }

    public List<String> getFullNamesByContinentName(String contName) {
        return getAll().
                stream()
                .filter(
                        x -> x.getCountry().getContinent().getContinent().equals(contName)
                )
                .map(CarMaker::getFullName)
                .collect(Collectors.toList());
    }
}
