package praxis.db.cars.repositories;

import praxis.db.cars.DataBaseConnection;
import praxis.db.cars.entities.Name;

import java.util.List;

public class NameRepository {
    private DataBaseConnection dbConnection;

    public NameRepository(DataBaseConnection dbConnection) {
        this.dbConnection = dbConnection;
    }

    public List<Name> getAll() {
        return dbConnection.getDBConnection().getAllNames();
    }
}
