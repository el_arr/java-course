package praxis.db.cars.repositories;

import praxis.db.cars.DataBaseConnection;
import praxis.db.cars.entities.CarData;

import java.util.List;

public class CarRepository {
    private DataBaseConnection dbConnection;

    public CarRepository(DataBaseConnection dbConnection) {
        this.dbConnection = dbConnection;
    }

    public List<CarData> getAll() {
        return dbConnection.getDBConnection().getAllCars();
    }
}
