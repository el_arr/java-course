package praxis.db.cars.repositories;

import praxis.db.cars.DataBaseConnection;
import praxis.db.cars.entities.Continent;

import java.util.LinkedList;
import java.util.List;

public class ContinentRepository {
    private DataBaseConnection dbConnection;

    public ContinentRepository(DataBaseConnection dbConnection) {
        this.dbConnection = dbConnection;
    }

    public List<Continent> getAll() {
        return dbConnection.getDBConnection().getAllContinents();
    }

    public Continent getById(int id) {
        return dbConnection.getDBConnection().getContinentById(id);
    }

    public Continent getByName(String name) {
        for (Continent continent : dbConnection.getDBConnection().getAllContinents()) {
            if (continent.getContinent().equals(name)) {
                return continent;
            }
        }
        return null;
    }
}
