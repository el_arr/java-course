package praxis.db.cars;

import praxis.db.cars.entities.CarData;
import praxis.db.cars.entities.CarDataComparator;
import praxis.db.cars.entities.Continent;
import praxis.db.cars.entities.Country;
import praxis.db.cars.repositories.*;

import java.io.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public class Main {
    private static ContinentRepository continentRepo;
    private static CountryRepository countryRepo;
    private static CarMakerRepository carMakerRepo;
    private static ModelRepository modelRepo;
    private static NameRepository nameRepo;
    private static CarRepository carRepo;

    private static ContinentRepository createContinentsRepo() {
        return new ContinentRepository(
                new DataBaseConnection()
        );
    }

    private static CountryRepository createCountryRepo() {
        return new CountryRepository(
                new DataBaseConnection()
        );
    }

    private static CarMakerRepository createCarMakerRepo() {
        return new CarMakerRepository(
                new DataBaseConnection()
        );
    }

    private static ModelRepository createModelRepo() {
        return new ModelRepository(
                new DataBaseConnection()
        );
    }

    private static NameRepository createNameRepo() {
        return new NameRepository(
                new DataBaseConnection()
        );
    }

    private static CarRepository createCarRepo() {
        return new CarRepository(
                new DataBaseConnection()
        );
    }

    private static void init() {
        continentRepo = createContinentsRepo();
        countryRepo = createCountryRepo();
        carMakerRepo = createCarMakerRepo();
        modelRepo = createModelRepo();
        nameRepo = createNameRepo();
        carRepo = createCarRepo();
    }

    public static void main(String[] args) throws NoSuchFieldException {
        init();
        task21();
    }

    private static void task0() {
        System.out.println(continentRepo.getAll());
        System.out.println(countryRepo.getAll());
        Continent continent = continentRepo.getByName("africa");
        Country country = countryRepo.getByName("usa");
        country.setContinent(continent);
        System.out.println(country);
    }

    private static void task1() {
        System.out.println(carMakerRepo.getFullNamesByContinentName("asia"));
    }

    private static void writeCarsData(List<CarData> data) {
        try (PrintWriter pw = new PrintWriter("res/sorted-cars-data.txt")) {
            Iterator<CarData> i = data.iterator();
            while (i.hasNext()) {
                pw.println(i.next() + "\n");
            }
        } catch (FileNotFoundException ex) {
            System.err.println("Ошибка! Файл не найден");
        }
    }

    private static void task21() {
        List<CarData> carsData = carRepo.getAll();
        System.out.println(carsData);
        carsData.sort(Comparator.naturalOrder());
        writeCarsData(carsData);
        System.out.println(carsData);
    }

    private static void task22() throws NoSuchFieldException {
        List<CarData> carsData = carRepo.getAll();
        carsData.sort(new CarDataComparator("mpg"));
        writeCarsData(carsData);
    }
}
