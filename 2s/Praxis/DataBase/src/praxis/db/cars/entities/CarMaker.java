package praxis.db.cars.entities;

public class CarMaker {
    private Integer id;
    private String maker;
    private String fullName;
    private Country country;
    private int countryId;

    public CarMaker(Integer id, String maker, String fullName, int countryId, Country country) {
        this.id = id;
        this.maker = maker;
        this.fullName = fullName;
        this.country = country;
        this.countryId = countryId;
    }

    public Integer getId() {
        return id;
    }

    public String getMaker() {
        return maker;
    }

    public String getFullName() {
        return fullName;
    }

    public Country getCountry() {
        return country;
    }

    public int getCountryId() {
        return countryId;
    }

    @Override
    public String toString() {
        return "CarMaker{ " +
                "id: " + id +
                ", maker: " + maker +
                ", fullName: " + fullName +
                ", country: " + country +
                ", countryId: " + countryId +
                " }" + '\n';
    }
}
