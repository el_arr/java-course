package praxis.db.cars.entities;

public class Continent {
    private Integer contId;
    private String continent;

    public Continent(Integer contId, String continent) {
        this.contId = contId;
        this.continent = continent;
    }

    public Integer getContId() {
        return contId;
    }

    public String getContinent() {
        return continent;
    }

    @Override
    public String toString() {
        return "Continent{ " +
                "contId: " + contId +
                ", continent: " + continent +
                " }";
    }
}
