package praxis.db.cars.entities;

public class CarData implements Comparable<CarData> {
    private int id;
    private Name name;
    private double mpg;
    private int cylinders;
    private double edispl;
    private int horsepower;
    private int weight;
    private double accelerate;
    private int year;

    public CarData() {
    }

    public CarData(int id, Name name, double mpg, int cylinders, double edispl, int horsepower, int weight, double accelerate, int year) {
        this.id = id;
        this.name = name;
        this.mpg = mpg;
        this.cylinders = cylinders;
        this.edispl = edispl;
        this.horsepower = horsepower;
        this.weight = weight;
        this.accelerate = accelerate;
        this.year = year;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Name getName() {
        return name;
    }

    public void setName(Name name) {
        this.name = name;
    }

    public double getMpg() {
        return mpg;
    }

    public void setMpg(double mpg) {
        this.mpg = mpg;
    }

    public int getCylinders() {
        return cylinders;
    }

    public void setCylinders(int cylinders) {
        this.cylinders = cylinders;
    }

    public double getEdispl() {
        return edispl;
    }

    public void setEdispl(double edispl) {
        this.edispl = edispl;
    }

    public int getHorsepower() {
        return horsepower;
    }

    public void setHorsepower(int horsepower) {
        this.horsepower = horsepower;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public double getAccelerate() {
        return accelerate;
    }

    public void setAccelerate(double accelerate) {
        this.accelerate = accelerate;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    @Override
    public String toString() {
        return "CarData{" +
                "id=" + id +
                ", name=" + name.getMake() +
                ", mpg=" + mpg +
                ", cylinders=" + cylinders +
                ", edispl=" + edispl +
                ", horsepower=" + horsepower +
                ", weight=" + weight +
                ", accelerate=" + accelerate +
                ", year=" + year +
                '}';
    }

    @Override
    public int compareTo(CarData o) {
        return this.id - o.getId();
    }
}
