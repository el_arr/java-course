package praxis.db.cars.entities;

public class Country {
    private Integer countryId;
    private String countryName;
    private Continent continent;
    private Integer continentId;

    public Country(Integer countryId, String countryName, Integer continentId) {
        this.countryId = countryId;
        this.countryName = countryName;
        this.continentId = continentId;
    }

    public Integer getCountryId() {
        return countryId;
    }

    public String getCountryName() {
        return countryName;
    }

    public Continent getContinent() {
        return continent;
    }

    public void setContinent(Continent continent) {
        this.continent = continent;
    }

    public Integer getContinentId() {
        return continentId;
    }

    @Override
    public String toString() {
        return "Country{ " +
                "countryId: " + countryId +
                ", countryName: " + countryName +
                ", continentId: " + continentId +
                " }";
    }
}
