package praxis.db.simple;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class DB {

    public static void task1() {
        Map<String, Integer> map = new HashMap<>();
        try {
            BufferedReader in = new BufferedReader(new FileReader(new File("res\\en_v1.dic")));
            try {
                String[] a;
                String s;
                while ((s = in.readLine()) != null) {
                    a = s.split(" ");
                    map.put(a[0], Integer.parseInt(a[1]));
                }
            } finally {
                in.close();
                System.out.println(map);
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    public static void task2() {
        Map<Integer, Set<String>> map = new TreeMap<>();
        try {
            BufferedReader in = new BufferedReader(new FileReader(new File("res\\en_v1.dic")));
            try {
                String[] a;
                String s;
                while ((s = in.readLine()) != null) {
                    a = s.split(" ");
                    Integer reg = Integer.parseInt(a[1]);
                    if (map.containsKey(reg)) {
                        map.get(reg).add(a[0]);
                    } else {
                        
                    }
                }
            } finally {
                in.close();
                System.out.println(map);
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    public static void task3() {
        LinkedList<Word> list = new LinkedList<>();
        try {
            BufferedReader in = new BufferedReader(new FileReader(new File("res\\en_v1.dic")));
            try {
                String[] a;
                String s;
                int i;
                while ((s = in.readLine()) != null) {
                    a = s.split(" ");
                    i = Integer.parseInt(a[1]);
                    list.add(new Word(a[0], i));
                }
            } finally {
                in.close();
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        Collections.sort(list, (Word o1, Word o2) -> {
                return o1.getWord().compareTo(o2.getWord());
        });
        System.out.println(list);
        Collections.sort(list, (Word o1, Word o2) -> {
            return o1.getFrequency().compareTo(o2.getFrequency());
        });
        System.out.println(list);
    }
}
