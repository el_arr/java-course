package praxis.db.simple;

public class Word {
    private String word;
    private Integer frequency;

    public Word() {}

    public Word(String word, int frequency) {
        this.word = word;
        this.frequency = frequency;
    }

    public String getWord() {
        return word;
    }

    public Integer getFrequency() {
        return frequency;
    }

}
