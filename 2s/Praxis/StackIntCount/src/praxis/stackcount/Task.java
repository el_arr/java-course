package praxis.stackcount;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Task {

    public static void main(String[] args) {
        String s = (new Scanner(System.in)).nextLine();
        System.out.println(infixToPostfix(s));
        //System.out.println(postfixStackCount(s));
        //System.out.println(postfixStackCount(infixToPostfix(s)));
    }

    public static String postfixStackCount(String s) {
        int first, second, res;
        LinkedIntStack stack = new LinkedIntStack();
        String[] a = s.split(",");
        Pattern p = Pattern.compile("([0-9]|([1-9]([0-9])*))");
        for (String st : a) {
            Matcher m = p.matcher(st);
            if (m.matches()) {
                stack.push(Integer.parseInt(st));
            } else {
                res = 0;
                first = stack.pop();
                second = stack.pop();
                if (st.equals("-")) {
                    res = first - second;
                } else if (st.equals("+")) {
                    res = first + second;
                } else if (st.equals("/")) {
                    res = first / second;
                } else if (st.equals("*")) {
                    res = first * second;
                }
                stack.push(res);
            }
        }
        return stack.toString();
    }

    public static String infixToPostfix(String s) {
        String res = "Азазаз, неправильно";
        transfer(res, s);
        return res;
    }

    private static String antiBrackets(String s) {
        int b1counter = 1;
        int b2counter = 0;
        int i = 0;
        while (b1counter != b2counter) {
            i++;
            if (s.charAt(i) == '(') {
                b1counter++;
            } else if (s.charAt(i) == ')') {
                b2counter++;
            }
        }
        transfer(result, s.substring(1, i - 1));
        transfer(result, s.substring(i + 1));
        return result;
    }

    private static void transfer(String result, String s) {
        Pattern p1 = Pattern.compile("\\d+");
        Pattern p2 = Pattern.compile("\\(");
        if (s.substring(0, 0).matches(p1.pattern())) {
            if (!result.equals("")) {
                result = result + ",";
            }
            result = result + s.charAt(1) + "," + s.charAt(0);
            transfer(result, s.substring(2));
        } else if (s.substring(0, 0).matches(p2.pattern())) {

        }
    }

}