package praxis.stackcount;

public class LinkedIntStack implements IIntStack {

    private class IntElem {
        private Integer value;
        private IntElem next;

        public IntElem(Integer value, IntElem next) {
            this.value = value;
            this.next = next;
        }

        public Integer getValue() {
            return value;
        }

        public IntElem getNext() {
            return next;
        }

        public String toString() {
            return value.toString();
        }
    }

    private IntElem head;

    public void push(Integer x) {
        head = new IntElem(x, head);
    }

    public Integer pop() {
        if (this.isEmpty()) {
            return null;
        }
        Integer c = head.getValue();
        head = head.getNext();
        return c;
    }

    public boolean isEmpty() {
        return head == null;
    }

    public String toString() {
        String res = "";
        IntElem t = head;
        while (t != null) {
            res = res + "\n" + t;
            t = t.getNext();
        }
        return res;
    }

}