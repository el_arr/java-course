package praxis.stackcount;

public interface IIntStack {

    void push(Integer x);

    boolean isEmpty();

    Integer pop();

}
