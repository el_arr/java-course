package praxis.game.armory;

import praxis.game.characters.Character;

public interface AttackArmory {

    void attack(Character enemy);

}
