package praxis.game.characters;

public abstract class Character {

    public abstract void attack(Character c);

}