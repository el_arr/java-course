package praxis.game.characters.enemy.creators;

import praxis.game.characters.EnemyCharacter;
import praxis.game.characters.enemy.Husk;

public class HuskCreator extends EnemyCharCreator{

    @Override
    public EnemyCharacter getNew() {
        return new Husk();
    }

}
