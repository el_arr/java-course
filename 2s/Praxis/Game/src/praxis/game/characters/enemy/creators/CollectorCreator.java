package praxis.game.characters.enemy.creators;

import praxis.game.characters.enemy.Collector;
import praxis.game.characters.EnemyCharacter;

public class CollectorCreator extends EnemyCharCreator{

    @Override
    public EnemyCharacter getNew() {
        return new Collector();
    }

}
