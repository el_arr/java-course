package praxis.game;

import praxis.game.armory.LaserWeapon;
import praxis.game.armory.Milieu;
import praxis.game.armory.MilieuToArmoryAdapter;
import praxis.game.characters.EnemyCharacter;
import praxis.game.characters.PlayerCharacter;
import praxis.game.characters.SuperPlayer;
import praxis.game.characters.enemy.creators.CollectorCreator;
import praxis.game.characters.enemy.creators.HuskCreator;

public class Game {

    public void startGame() {
        PlayerCharacter player = new PlayerCharacter();
        LaserWeapon wp = new LaserWeapon();
        player.setCurrentArmory(wp);

        CollectorCreator cc = new CollectorCreator();
        EnemyCharacter c1 = cc.getNew();

        HuskCreator hc = new HuskCreator();
        EnemyCharacter h1 = hc.getNew();

        player.attack(c1);
        player.attack(h1);

        Milieu m = new Milieu();

        player.setCurrentArmory(new MilieuToArmoryAdapter(m));

        SuperPlayer sp = new SuperPlayer(player);
        sp.attack(h1);
        sp.healSelf();
    }

    public static void main(String[] args) {
        (new Game()).startGame();
    }

}
