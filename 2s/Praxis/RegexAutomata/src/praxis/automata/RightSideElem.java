package praxis.automata;

public class RightSideElem implements Elem {
    private Character terminal;
    private String nonTerminal;

    public RightSideElem(Character terminal, String nonTerminal) {
        this.terminal = terminal;
        this.nonTerminal = nonTerminal;
    }

    public String toString() {
        return "T:" + terminal + ";NT:" + nonTerminal;
    }

    public Character getTerminal() {
        return terminal;
    }

    public String getNonTerminal() {
        return nonTerminal;
    }
}
