package praxis.automata;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class NFA {
    private Map<InputAndStateNFA, List<State>> transitions = new TreeMap<>();
    private State finalStates = new State("LoL".hashCode() + "");
    private State startState;
    private String alphabet;

    public NFA(Grammar g) {
        startState = new State(g.getStartNonTerminal());
        Map<String, List<RightSideElem>> pr = g.getProduction();
        for (String leftSide : pr.keySet()) {
            State from = new State(leftSide);
            for (RightSideElem rse : pr.get(leftSide)) {
                State to;
                Character c = rse.getTerminal();
                InputAndStateNFA ias = new InputAndStateNFA(c, from);
                if (rse.getNonTerminal().equals("")) {
                    to = finalStates;
                } else {
                    to = new State(rse.getNonTerminal());
                }
                if (transitions.containsKey(ias)) {
                    List<State> lst = transitions.get(ias);
                    lst.add(to);
                } else {
                    List<State> lst = new ArrayList<>();
                    lst.add(to);
                    transitions.put(ias, lst);
                }
            }
        }
    }

    @Override
    public String toString() {
        return "NFA{" + "\n" +
                "transitions: " + transitions + "\n" +
                "finalStates: " + finalStates + "\n" +
                "startState: " + startState + "\n" +
                "}";
    }

    public Map<InputAndStateNFA, List<State>> getTransitions() {
        return transitions;
    }

    public State getFinalStates() {
        return finalStates;
    }

    public State getStartState() {
        return startState;
    }

    public String getAlphabet() {
        return alphabet;
    }
}