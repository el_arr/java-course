package praxis.automata;

public class State implements Comparable {
    private String state;

    public State(String state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return "State{" +
                "state='" + state + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        return (obj instanceof State) && state.equals(((State) obj).state);
    }

    public String getState() {
        return state;
    }

    @Override
    public int compareTo(Object o) {
        return state.compareTo(((State) o).getState());
    }
}
