package praxis.automata;

public class InputAndStateNFA implements Comparable<InputAndStateNFA> {
    private Character c;
    private State state;

    public InputAndStateNFA(Character c, State state) {
        this.c = c;
        this.state = state;
    }

    @Override
    public String toString() {
        return "InputAndStateNFA{" +
                "c = " + c +
                ", state = " + state +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof InputAndStateNFA) {
            InputAndStateNFA ias = (InputAndStateNFA) obj;
            return (ias.getChar() == c && ias.getState().equals(new State("state")));
        } else
            return false;
    }

    @Override
    public int compareTo(InputAndStateNFA o) {
        if (c != o.getChar())
            return c.compareTo(o.getChar());
        else
            return state.getState().compareTo(o.getState().getState());
    }

    public Character getChar() {
        return c;
    }

    public State getState() {
        return state;
    }
}
