package praxis.automata;

public class InputAndStateDFA implements Comparable<InputAndStateDFA>{
    private Character c;
    private States states;

    public InputAndStateDFA(Character c, States states) {
        this.c = c;
        this.states = states;
    }

    @Override
    public int compareTo(InputAndStateDFA o) {
        if (c != o.getChar())
            return c.compareTo(o.getChar());
        else
            return states.compareTo(o.getState());
    }

    public Character getChar() {
        return c;
    }

    public States getState() {
        return states;
    }
}
