package praxis.automata;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class Grammar {
    private String filename = "grammar.txt";
    private String startNonTerminal;
    private String terminals;
    private List<String> nonTerminals;
    private Map<String, List<RightSideElem>> production;

    public Grammar() {
        readGrammar(filename);
    }

    private void readGrammar(String filename) {
        File f = new File("res\\" + filename);
        String s;
        try {
            Scanner in = new Scanner(f);
            terminals = in.nextLine();
            nonTerminals = Arrays.asList(in.nextLine().split(" "));
            startNonTerminal = in.nextLine();
            production = new HashMap<>();
            while (in.hasNextLine()) {
                String[] args = in.nextLine().split(" -> ");
                String[] rightSides = args[1].split("\\|");
                List<RightSideElem> t = Arrays.stream(rightSides)
                        .map((x) -> new RightSideElem(x.charAt(0), x.substring(1)))
                        .collect(Collectors.toList());
                production.put(args[0], t);
            }
            in.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public String toString() {
        return "Grammar{" + "\n" +
                "startNonTerminal: '" + startNonTerminal + '\'' + '\n' +
                "terminals: '" + terminals + '\'' + '\n' +
                "nonTerminals: " + nonTerminals + '\n' +
                "production: " + production + '\n' +
                '}';
    }

    public String getStartNonTerminal() {
        return startNonTerminal;
    }

    public Map<String, List<RightSideElem>> getProduction() {
        return production;
    }
}
