package praxis.automata;

public class RegexChecker {

    boolean checkByRegex(String rexeg, String input) {
        return input.matches(rexeg);
    }

}