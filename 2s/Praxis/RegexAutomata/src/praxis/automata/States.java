package praxis.automata;

import java.util.Arrays;
import java.util.TreeSet;

public class States extends TreeSet<State>
        implements Comparable<States> {

    @Override
    public int compareTo(States o) {
        if (Arrays.equals(this.toArray(), o.toArray()))
            return 0;
        else
            return this.size() - o.size();
    }

}
