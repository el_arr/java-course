package praxis.automata;

public class StateMachine {

    boolean checkByStatesTask1(String input) {
        char state = 's';
        for (int i : input.toCharArray()) {
            if (state == 's') {
                if (i == 0 || i == 1) {
                    state = 'a';
                }
            } else if (i == 1) {
                return false;
            }
        }
        return true;
    }

    boolean checkByStatesTask2(String input) {
        char state = 's';
        for (int i : input.toCharArray()) {
            switch (state) {
                case 's':
                    if (i == 0) {
                        state = 'a';
                        break;
                    } else
                        return false;
                case 'a':
                    if (i == 1) {
                        state = 'b';
                        break;
                    }
                case 'b':
                    if (i == 0) {
                        return false;
                    }
            }
        }
        return true;
    }
}