/**
* @author Eldar Mingachev
* 11-601
* KR1, Task2Б
*/	

import java.util.Scanner;

public class Task2B{

	public static void main(String [] args){
		
		Scanner in = new Scanner(System.in);
		
		System.out.println("Введите N");
		int n = in.nextInt();
		int [] a = new int[n];
		int i = 0;
		int inct = 0;
		int ct = 0;
		int c,d,cur;
		boolean infl = true;
		boolean fl = true;
		for (i = 0; i < n; i++) {
			System.out.println("Введите элемент массива");
			a[i] = in.nextInt();
		}
		for (i = 0; (i < n)&&(fl); i++) {
			cur = a[i];
			infl = true;
			while ((cur>0)&&(infl)){
				d = cur%10;
				if (d%2 == 1) {
					inct++;
					if (inct == 2) {
						infl = false;
					}
				}
				cur = cur/10;
			}
			if (!infl) {
				ct++;
				if (ct >3) {
					fl = false;
				}
			}
		}	
		if (fl){
			System.out.println("В массиве не более 3х таких чисел (" + ct +")");
		}
		else{
			System.out.println("В массиве более 3х таких чисел (" + ct +")");
		}	
	
	}
}