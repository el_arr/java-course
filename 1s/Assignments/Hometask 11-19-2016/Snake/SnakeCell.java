/**
* @author Eldar Mingachev
* 11-601
* HW 19.11.2016, Task 4
*/

public class SnakeCell {
	private int x,y;

	public SnakeCell(){

	}

	public SnakeCell(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public void setX(int x) {
		this.x = x;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getX() {
		return this.x;
	}

	public int getY() {
		return this.y;
	}

	public boolean equals(Food f) {
		if ((f.getX() == this.x) && (f.getY() == this.y)) {
			return true;
		}
		else {
			return false;
		}
	}

	public boolean equals(SnakeCell c) {
		if ((c.getX() == this.x) && (c.getY() == this.y)) {
			return true;
		}
		else {
			return false;
		}
	}

	public String toString() {
		return x + " " + y;
	}

}