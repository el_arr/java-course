/**
* @author Eldar Mingachev
* 11-601
* HW 19.11.2016, Task 4
*/

import java.util.Scanner;

public class Game {

	public static void main(String[] args) {
		play();
	}

	public static void play() {
		Scanner in = new Scanner(System.in);
		boolean check = true;
		boolean playing = true;
		boolean interaction = true;
		boolean input_mismatch = false;
		boolean food_eaten = false;
		String input, choice;
		int movement = -1;
		SnakeCell t = new SnakeCell();
		System.out.println("Welcome to the Snake." + "\n" + 
			"How to play:" + "\n" + 
			"Use keys 'W','A','S','D' to direct your snake" + "\n" + 
			"Eat food (X) and grow in size" + "\n" +
			"Get as big as you can");
		while (interaction) {
			if (check) {
				Field field = new Field();
				Snake snake = new Snake();
				Food food = new Food(snake);
				while (playing) {
					if (!input_mismatch && food_eaten) {
						food = new Food(snake);
					}
					food_eaten = false;
					field = new Field();
					field.render(snake);
					field.render(food);
					field.show();
					input = in.next().toUpperCase();
				//Проверка ввода. Формат - WASD, иначе - новая итерация, новая попытка ввода.
					switch (input) {
					case "W":
						input_mismatch = false;
						movement = 0;
						break;	
					case "A":
						input_mismatch = false;
						movement = 1;
						break;
					case "S":
						input_mismatch = false;
						movement = 2;
						break;
					case "D":
						input_mismatch = false;
						movement = 3;
						break;
					default:
						input_mismatch = true;
						System.out.println("Input is incorrect. Please, enter the direction with keys:" + "\n" + "'W','A','S','D'");
					}
					if (input_mismatch == true) {
						continue;
					}
				//Обработка движений
					//Проверка, является ли следующая выбранная клетка - едой
					switch (movement) {
					case 0:
						t.setX(snake.getCell(0).getX() - 1);
						break;
					case 1:
						t.setY(snake.getCell(0).getY() - 1);
						break;
					case 2:
						t.setX(snake.getCell(0).getX() + 1);
						break;
					case 3:
						t.setY(snake.getCell(0).getY() + 1);
						break;
					}
					snake.move(movement);
					if (t.equals(food)) {
						snake.eat(food);
						food_eaten = true;
						continue;
					}
				//Условия выхода:
					//Змейка врезается в себя.
					for (int i = 1; i < snake.getLength(); i++) {
						if (snake.getCell(0).equals(snake.getCell(i))) {
							playing = false;
							System.out.println("Cannibalism");
							for (int j = 0; j < snake.getLength(); j++) {
								System.out.println(snake.getCell(i));
							}
						}
					}
					//Выход за стены.
					if ((snake.getCell(0).getX() < 0) || (snake.getCell(0).getX() >= 10) || (snake.getCell(0).getY() < 0) || (snake.getCell(0).getY() >= 10)) {
						playing = false;
						System.out.println("out of bounds");
					}
					//Размер = размеру поля.
					if (snake.getLength() == 100) {
						playing = false;
					}
				}

				if (snake.getLength() == 100) {
					System.out.println("Congratulations!" + "\n" + "You've won!");
				}
				else {
					System.out.println("My apologies." + "\n" + "You've lost!");
				}

				System.out.println("Would you like to play again?");
			}
			System.out.println("Y / N ?");
			choice = in.next().toUpperCase();
		//Choice checking.
			switch (choice) {
			case "Y":
				playing = true;
				interaction = true;
				check = true;
				break;
			case "N":
				check = true;
				interaction = false;
				break;
			default:
				check = false;
				System.out.println("Input is incorrect. Please, try again" + "\n");
			}		
		}
	}
}