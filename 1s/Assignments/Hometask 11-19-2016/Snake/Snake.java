/**
* @author Eldar Mingachev
* 11-601
* HW 19.11.2016, Task 4
*/

public class Snake {
	private int length = 0;
	private SnakeCell [] c = new SnakeCell[100];
	private SnakeCell spareEnd;

	public Snake() {
		SnakeCell n0 = new SnakeCell(0,0);
		this.c[0] = n0;
		this.length = 1;
	}

	public void move(int movement) {
		spareEnd = c[length - 1];
		for (int i = length - 1; i >= 1 ; i--) {
			c[i] = c[i-1];
		}
		switch (movement) {
			case 0:
				c[0].setX(c[0].getX() - 1);
				break;
			case 1:
				c[0].setY(c[0].getY() - 1);
				break;
			case 2:
				c[0].setX(c[0].getX() + 1);
				break;
			case 3:
				c[0].setY(c[0].getY() + 1);
				break;
		}
	}

	public void eat(Food f) {
		length++;
		c[length - 1] = spareEnd;
	}

	public int getLength() {
		return this.length;
	}

	public SnakeCell getCell(int i) {
		return this.c[i];
	}
/*
	public String toString() {
		String res = "";
		for (int i = 0; i < length; i++) {
			res = "" + c[i];
		}
		return res;
	}
*/
}