/**
* @author Eldar Mingachev
* 11-601
* 004, 003
*/

package ru.kpfu.itis.el_arr.util;

public class RationalFraction {
	private int n, d;

	public RationalFraction() {
		this.n = 0;
		this.d = 1;
	}
	
	public RationalFraction(int n, int d) {
		this.n = n;
		this.d = d;
	}

	public void setN(int n) {
		this.n = n;
	}

	public int getN() {
		return n;
	}

	public void setD(int d) {
		this.d = d;
	}

	public int getD() {
		return d;
	}

	public void reduce() {
		for (int i = Math.abs(this.n); i > 0 ; i--) {
			if ((this.n % i == 0) && (this.d % i == 0)) {
				this.n /= i;
				this.d /= i;
			}
		}
	}

	public RationalFraction add(RationalFraction fr) {
		RationalFraction result = new RationalFraction();
		if (this.d == fr.getD()) {
			result.setN(this.n + fr.getN());
		}
		else {
			result.setD(this.d * fr.getD());
			result.setN(this.n * fr.getD() + fr.getN() * this.d);
		}
		result.reduce();
		return result;
	}

	public void add2(RationalFraction fr) {
		if (this.d == fr.getD()) {
			this.n += fr.getN();
		}
		else {
			this.d *= fr.getD();
			this.n = (this.n * fr.getD()) + (fr.getN() * this.d);
		}
		this.reduce();
	}

	public RationalFraction sub(RationalFraction fr) {
		RationalFraction result = new RationalFraction();
		if (this.d == fr.getD()) {
			result.setN(this.n - fr.getN());
		}
		else {
			result.setD(this.d * fr.getD());
			result.setN(this.n * fr.getD() - fr.getN() * this.d);
		}
		if ((result.getN() < 0) && (result.getD() < 0)) {
			result.setN(-1 * this.n);
			result.setD(-1 * this.d);
		}
		result.reduce();
		return result;
	}

	public void sub2(RationalFraction fr) {
		if (this.d == fr.getD()) {
			this.n += fr.getN();
		}
		else {
			this.d *= fr.getD();
			this.n = this.n * fr.getD() + fr.getN() * this.d;
		}
		this.reduce();
	}

	public RationalFraction mult(RationalFraction fr) {
		RationalFraction result = new RationalFraction();
		result.setN(this.n * fr.getN());
		result.setD(this.d * fr.getD());
		result.reduce();
		return result;
	}

	public void mult2(RationalFraction fr) {
		this.n *= fr.getN();
		this.d *= fr.getD();
		this.reduce();
	}

	public RationalFraction div(RationalFraction fr) {
		RationalFraction result = new RationalFraction();
		result.setN(this.n * fr.getD());
		result.setD(this.d * fr.getN());
		result.reduce();
		return result;
	}

	public void div2(RationalFraction fr) {
		this.n *= fr.getD();
		this.d *= fr.getN();
		this.reduce();
	}

	public double value() {
		double n = this.n;
		double d = this.d;
		double result = n / d;
		return result;
	}

	public int numberPart() {
		int result = this.n / this.d;
		return result;
	}

	public boolean equals(RationalFraction fr2) {
		RationalFraction fr1 = new RationalFraction(this.n, this.d);
		fr1.reduce();
		fr2.reduce();
		if ((fr1.getN() == fr2.getN()) && (fr1.getD() == fr2.getD())) {
			return true;
		}
		else
			return false;
	}

	public String toString() {
		if (this.d == 0) {
			return "Неправильный ввод, знаменатель = 0";
		}
		else {
			if (this.n == 0) {
				return "0";
			}
			else {
				if (this.d == 1) {
					return "" + this.n;
				}
				else {
					return this.n + "/" + this.d;
				}
			}
		}
	}

	public static void main() {
		RationalFraction fr1 = new RationalFraction();
		fr1.setN(1);
		fr1.setD(2);
		
		RationalFraction fr2 = new RationalFraction();
		fr2.setN(3);
		fr2.setD(4);

		RationalFraction fr3 = fr1.add(fr2);
		System.out.println("Add (fr3=fr1+fr2):" + "\n" + fr3 + "\n");
		
		fr3.add2(fr2);
		System.out.println("Add2 (fr3+fr2):" + "\n" + fr3 + "\n");
		
		fr3 = fr1.sub(fr2);
		System.out.println("Sub (fr3=fr1-fr2):" + "\n" + fr3 + "\n");
		
		fr3.sub2(fr2);
		System.out.println("Sub2 (fr3-fr2):" + "\n" + fr3 + "\n");
		
		fr3 = fr1.mult(fr2);
		System.out.println("Mult (fr3=fr1*fr2):" + "\n" + fr3 + "\n");
		
		fr3.mult2(fr2);
		System.out.println("Mult2 (fr3*fr2):" + "\n" + fr3 + "\n");
		
		fr3 = fr1.div(fr2);
		System.out.println("Div (fr3=fr1/fr2):" + "\n" + fr3 + "\n");
		
		fr3.div2(fr2);
		System.out.println("Div2 (fr3/fr2):" + "\n" + fr3 + "\n");
		
		System.out.println("Value of fr1:" + "\n" + fr1.value() + "\n");
		
		RationalFraction fr4 = new RationalFraction(33,22);
		System.out.println("Number part of fr4(33,22):" + "\n" + fr4.numberPart() + "\n");
		
		for (int i = 0; i < 2; i++) {
			if (i == 1) {
				fr1.setN(1);
				fr1.setD(2);
				fr2.setN(1);
				fr2.setD(2);
				System.out.println("For N1 = N2 = 1, D1 = D2 = 2:");
			}
		
			if (fr1.equals(fr2)) {
				System.out.println("fr1 equals fr2");
			}
			else
				System.out.println("fr1 doesn't equal fr2" + "\n");
		}
	}
}