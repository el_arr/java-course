/**
* @author Eldar Mingachev
* 11-601
* 004, 005
*/

package ru.kpfu.itis.el_arr.util;

public class Matrix2x2 {
	private double [][] matrix = new double[2][2];

	public Matrix2x2() {
		this(0,0,0,0);
	}
	
	public Matrix2x2(double [][] matrix) {
		this.matrix = matrix;
	}

	public Matrix2x2(double a,double b,double c,double d) {
		matrix[0][0] = a;
		matrix[0][1] = b;
		matrix[1][0] = c;
		matrix[1][1] = d;
	}

	public Matrix2x2 add(Matrix2x2 m) {
		Matrix2x2 result = new Matrix2x2();
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				result.matrix[i][j] = this.matrix[i][j] + m.matrix[i][j];
			}
		}
		return result;
	}

	public void add2(Matrix2x2 m) {
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				this.matrix[i][j] += m.matrix[i][j];
			}
		}
	}
	
	public Matrix2x2 sub(Matrix2x2 m) {
		Matrix2x2 result = new Matrix2x2();
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				result.matrix[i][j] = this.matrix[i][j] - m.matrix[i][j];
			}
		}
		return result;
	}

	public void sub2(Matrix2x2 m) {
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				this.matrix[i][j] -= m.matrix[i][j];
			}
		}
	}

	public Matrix2x2 multNumber(double n) {
		Matrix2x2 result = new Matrix2x2();
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				result.matrix[i][j] = this.matrix[i][j] * n;
			}
		}
		return result;
	}

	public void multNumber2(double n) {
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				this.matrix[i][j] *= n;
			}
		}
	}

	public Matrix2x2 mult(Matrix2x2 m) {
		Matrix2x2 result = new Matrix2x2();
		double c;
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				c = 0;
				for (int k = 0; k < matrix[i].length; k++) {
					c += matrix[i][k] * m.matrix[k][j];
				}
				result.matrix[i][j] = c;
			}
		}
		return result;
	}

	public void mult2(Matrix2x2 m) {
		double c;
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				c = 0;
				for (int k = 0; k < matrix[i].length; k++) {
					c += matrix[i][k] * m.matrix[k][j];
				}
				this.matrix[i][j] = c;
			}
		}
	}

	public double det() {
		return matrix[0][0] * matrix[1][1] - matrix[0][1] * matrix[1][0];
	}
	
	public void transpon() {
		double t;
		double [][] m = new double[2][2];
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				m[i][j] = this.matrix[j][i];
			}
		}
		this.matrix = m;
	}

	public Matrix2x2 inverseMatrix() {
		Matrix2x2 result = new Matrix2x2();
		if (this.det() == 0) {
			System.out.println("Ошибка: определитель равен нулю");
			result.matrix[0][0] = 0;
			result.matrix[0][1] = 0;
			result.matrix[1][0] = 0;
			result.matrix[1][1] = 0;
		}
		else {
		result.matrix[0][0] = matrix[1][1];
		result.matrix[0][1] = -1 * matrix[0][1];
		result.matrix[1][0] = -1 * matrix[1][0];
		result.matrix[1][1] = matrix[0][0];
		}
		return result;
	}

	public Matrix2x2 equivalentDiagonal() {
		Matrix2x2 result = new Matrix2x2();
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				if (i == j) {
					result.matrix[i][j] = 1;
				}
				else
					result.matrix[i][j] = 0;
			}
		}
		return result;
	}

	public Vector2D multVector(double x, double y) {
		Vector2D result = new Vector2D(x,y);
		double [][] matrix = this.matrix;
		double c;
		double t = 0;
		for (int i = 0; i < matrix.length; i++) {
			c = 0;
			for (int j = 0; j < matrix[i].length; j++) {
				if (j == 0) {
					t = x;
				}
				else 
					if (j == 1) {
						t = y;
					}
				c += matrix[i][j] * t;
			}
			if (i == 0) {
				result.setX(c);
			}
			else 
				if (i == 1) {
					result.setY(c);
				} 
		}
		return result;
	}
		
	public String toString() {
		String str = "";
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				str += matrix[i][j] + " ";
			}
			str += "\n";
		}
		return str;
	}

	public static void main() {
		Matrix2x2 m1 = new Matrix2x2(new double[][] {
			{1,2},
			{3,4}
		});

		Matrix2x2 m2 = new Matrix2x2(new double[][] {
			{1,0},
			{0,1}
		});

		System.out.println("m1:" + "\n" + m1);

		Matrix2x2 m3 = m1.add(m2);
		System.out.println("Add (m3=m1+m2):" + "\n" + m3 + "\n");
		
		m3.add2(m2);
		System.out.println("Add2 (m3+=m2):" + "\n" + m3 + "\n");
		
		m3 = m1.sub(m2);
		System.out.println("Sub (m3=m1-m2):" + "\n" + m3 + "\n");
		
		m3.sub2(m2);
		System.out.println("Sub2 (m3-=m2):" + "\n" + m3 + "\n");
		
		m3 = m1.multNumber(2);
		System.out.println("MultNum (m3=m1*2):" + "\n" + m3 + "\n");
		
		m3.multNumber2(2);
		System.out.println("MultNum2 (m3*=2):" + "\n" + m3 + "\n");
		
		m3 = m1.mult(m2);
		System.out.println("Mult (m3=m1*m2):" + "\n" + m3 + "\n");
		
		m3.mult2(m2);
		System.out.println("Mult2 (m3*=m2):" + "\n" + m3 + "\n");
		
		System.out.println("Det of m1:" + "\n" + m1.det() + "\n");
		
		m1.transpon();

		System.out.println("Transpon of m1:" + "\n" + m1 + "\n");

		System.out.println("Inverse of m1:" + "\n" + m1.inverseMatrix() + "\n");
		
		System.out.println("EquivalentDiagonal m3:" + "\n" + m3.equivalentDiagonal() + "\n");
		
		Vector2D v1 = m1.multVector(3,5);
		System.out.println("m1 multVector [3,5]:" + "\n" + v1 + "\n");
	}
}