/**
* @author Eldar Mingachev
* 11-601
* 004, 004
*/

package ru.kpfu.itis.el_arr.util;

public class ComplexNumber {
	private double a, b;

	public ComplexNumber() {
		this.a = 0;
		this.b = 0;
	}
	
	public ComplexNumber(double a, double b) {
		this.a = a;
		this.b = b;
	}

	public void setA(double a) {
		this.a = a;
	}

	public double getA() {
		return a;
	}

	public void setB(double b) {
		this.b = b;
	}

	public double getB() {
		return b;
	}

	public ComplexNumber add(ComplexNumber n) {
		ComplexNumber result = new ComplexNumber((a + n.getA()), (b + n.getB()));
		return result;
	}

	public void add2(ComplexNumber n) {
		this.a += n.getA();
		this.b += n.getB();
	}

	public ComplexNumber sub(ComplexNumber n) {
		ComplexNumber result = new ComplexNumber((a - n.getA()), (b - n.getB()));
		return result;
	}

	public void sub2(ComplexNumber n) {
		this.a -= n.getA();
		this.b -= n.getB();
	}

	public ComplexNumber multNumber(double t) {
		ComplexNumber result = new ComplexNumber((this.a * t), (this.b * t));
		return result;
	}

	public void multNumber2(double t) {
		this.a *= t;
		this.b *= t;
	}
	
	public ComplexNumber mult(ComplexNumber n) {
		ComplexNumber result = new ComplexNumber((this.a * n.getA() - this.b * n.getB()),(this.a * n.getB() + this.b * n.getA()));
		return result;
	}

	public void mult2(ComplexNumber n) {
		double a = this.a;
		double b = this.b;
		this.a = a * n.getA() - b * n.getB();
		this.b = a * n.getB() + b * n.getA();
	}

	public ComplexNumber div(ComplexNumber n) {
		double a = this.a;
		double b = this.b;
		double a1 = (a * n.getA() + b * n.getB()) / (n.getA() * n.getA() + n.getB() * n.getB());
		double b1 = (b * n.getA() - a * n.getB()) / (n.getA() * n.getA() + n.getB() * n.getB());
		ComplexNumber result = new ComplexNumber(a1,b1);
		return result;
	}

	public void div2(ComplexNumber n) {
		double a = this.a;
		double b = this.b;
		this.a = (a * n.getA() + b * n.getB()) / (n.getA() * n.getA() + n.getB() * n.getB());
		this.b = (b * n.getA() - a * n.getB()) / (n.getA() * n.getA() + n.getB() * n.getB());
	}

	public double length() {
		double result = Math.sqrt(a*a + b*b);
		return result;
	}

	public double arg() {
		double result = Math.atan(this.b/this.a);
		return result;
	}

	public ComplexNumber pow(double p) {
		double r = Math.pow(Math.sqrt(this.a * this.a + this.b * this.b), p);
		ComplexNumber result = new ComplexNumber();
		result.setA(r * Math.cos(p * this.arg()));
		result.setB(r * Math.sin(p * this.arg()));
		return result;
	}

	public boolean equals(ComplexNumber n) {
		if ((this.a == n.getA()) && (this.b == n.getB())) {
			return true;
		}
		else 
			return false;
	}

	public String toString() {
		if (a < 0) {
			return "" + b + " * i - " + Math.abs(a);
		}
		else {
			if (a == 0) {
				return "" + b + " * i";
			}
			else
				return  "" + b + " * i + " + a;
		}
	}

	public static void main() {
		ComplexNumber n1 = new ComplexNumber();
		n1.setA(1);
		n1.setB(2);
		
		ComplexNumber n2 = new ComplexNumber();
		n2.setA(3);
		n2.setB(4);

		ComplexNumber n3 = n1.add(n2);
		System.out.println("Add:" + "\n" + n3 + "\n");
		
		n3.add2(n2);
		System.out.println("Add2:" + "\n" + n3 + "\n");
		
		n3 = n1.sub(n2);
		System.out.println("Sub:" + "\n" + n3 + "\n");
		
		n3.sub2(n2);
		System.out.println("Sub2:" + "\n" + n3 + "\n");
		
		n3 = n1.multNumber(2);
		System.out.println("Mult(n1*2):" + "\n" + n3 + "\n");
		
		n3.multNumber2(2);
		System.out.println("Mult2:" + "\n" + n3 + "\n");
		
		n3 = n1.mult(n2);
		System.out.println("Mult:" + "\n" + n3 + "\n");
		
		n3.mult(n2);
		System.out.println("Mult2:" + "\n" + n3 + "\n");

		n3 = n1.div(n2);
		System.out.println("Div:" + "\n" + n3 + "\n");
		
		n3.div2(n2);
		System.out.println("Div2:" + "\n" + n3 + "\n");

		System.out.println("Length of n3:" + "\n" + n3.length() + "\n");
		
		System.out.println("Arg of n1:" + "\n" + n1.arg() + "\n");
		
		n3 = n1.pow(3);
		System.out.println("n1.pow(3):" + "\n" + n3 + "\n");
		
		for (int i = 0; i < 2; i++) {
			if (i == 1) {
				n1.setA(1);
				n1.setB(2);
				n2.setA(1);
				n2.setB(2);
				System.out.println("For a1 = a2 = 1, b1 = b2 = 2:");
			}
		
			if (n1.equals(n2)) {
				System.out.println("n1 equals n2");
			}
			else
				System.out.println("n1 doesn't equal n2" + "\n");
		}
	}
}