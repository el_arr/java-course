/**
* @author Eldar Mingachev
* 11-601
* 004, 011
*/

package ru.kpfu.itis.el_arr.util;

public class RationalComplexVector2D {
	private RationalComplexNumber x, y;

	public RationalComplexVector2D() {
		this.x = new RationalComplexNumber();
		this.y = new RationalComplexNumber();
	}
	
	public RationalComplexVector2D(RationalComplexNumber x, RationalComplexNumber y) {
		this.x = x;
		this.y = y;
	}

	public void setX(RationalComplexNumber x) {
		this.x = x;
	}

	public RationalComplexNumber getX() {
		return x;
	}

	public void setY(RationalComplexNumber y) {
		this.y = y;
	}

	public RationalComplexNumber getY() {
		return y;
	}

	public RationalComplexVector2D add(RationalComplexVector2D rcv) {
		RationalComplexVector2D result = new RationalComplexVector2D(this.x.add(rcv.getX()), this.y.add(rcv.getY()));
		return result;
	}

	public RationalComplexNumber scalarProduct(RationalComplexVector2D rcv) {
		return (this.x.mult(rcv.getX())).add(this.y.mult(rcv.getY()));
	}

	public String toString() {
		return "<" + x + ", " + y + ">";
	}

	public static void main() {
		RationalFraction ax1 = new RationalFraction(1,2);
		RationalFraction bx1 = new RationalFraction(3,4);
		RationalComplexNumber x1 = new RationalComplexNumber(ax1,bx1);
		
		RationalFraction ay1 = new RationalFraction(5,6);
		RationalFraction by1 = new RationalFraction(7,8);
		RationalComplexNumber y1 = new RationalComplexNumber(ay1,by1);
		
		RationalComplexVector2D rcv1 = new RationalComplexVector2D(x1,y1);
		System.out.println("RCV1:" + "\n" + rcv1 + "\n");
		
		RationalFraction ax2 = new RationalFraction(5,6);
		RationalFraction bx2 = new RationalFraction(7,8);
		RationalComplexNumber x2 = new RationalComplexNumber(ax2,bx2);
		
		RationalFraction ay2 = new RationalFraction(9,10);
		RationalFraction by2 = new RationalFraction(11,12);
		RationalComplexNumber y2 = new RationalComplexNumber(ay2,by2);		

		RationalComplexVector2D rcv2 = new RationalComplexVector2D(x2,y2);
		System.out.println("RCV2:" + "\n" + rcv2 + "\n");
		
		RationalComplexVector2D rcv3 = rcv1.add(rcv2);
		System.out.println("Add:" + "\n" + rcv3 + "\n");
	
		RationalComplexNumber rcn = rcv1.scalarProduct(rcv2);
		System.out.println("\n" + "ScalarProduct: " + "\n" + rcn);
	}

}