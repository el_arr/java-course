/**
* @author Eldar Mingachev
* 11-601
* 004, 010
*/

package ru.kpfu.itis.el_arr.util;

public class RationalComplexNumber {
	private RationalFraction a, b;

	public RationalComplexNumber() {
		this.a = new RationalFraction();
		this.b = new RationalFraction();
	}
	
	public RationalComplexNumber(RationalFraction a, RationalFraction b) {
		this.a = a;
		this.b = b;
	}

	public void setA(RationalFraction a) {
		this.a = a;
	}

	public RationalFraction getA() {
		return a;
	}

	public void setB(RationalFraction b) {
		this.b = b;
	}

	public RationalFraction getB() {
		return b;
	}

	public RationalComplexNumber add(RationalComplexNumber cn) {
		RationalComplexNumber result = new RationalComplexNumber(this.a.add(cn.getA()), this.b.add(cn.getB()));
		return result;
	}

	public RationalComplexNumber sub(RationalComplexNumber cn) {
		RationalComplexNumber result = new RationalComplexNumber(this.a.sub(cn.getA()), this.b.sub(cn.getB()));
		return result;
	}

	public RationalComplexNumber mult(RationalComplexNumber cn) {
		RationalComplexNumber result = new RationalComplexNumber(this.a.mult(cn.getA()).sub(this.b.mult(cn.getB())), (this.a.mult(cn.getB())).add(this.b.mult(cn.getA())));
		return result;
	}

	public String toString() {
		RationalFraction n = new RationalFraction();
		if (this.a.value() < 0) {
			return "" + b + " * i " + a;
		}
		else {
			if (this.a.value() == 0) {
				return "" + b + " * i";
			}
			else
				return  "" + b + " * i + " + a;
		}
	}

	public static void main() {
		RationalFraction a1 = new RationalFraction(1,2);
		RationalFraction b1 = new RationalFraction(3,4);
		RationalComplexNumber rcn1 = new RationalComplexNumber(a1,b1);
		System.out.println("RCN1:" + "\n" + rcn1 + "\n");
		
		RationalFraction a2 = new RationalFraction(5,6);
		RationalFraction b2 = new RationalFraction(7,8);
		RationalComplexNumber rcn2 = new RationalComplexNumber(a2,b2);
		System.out.println("RCN2:" + "\n" + rcn2 + "\n");
		
		RationalComplexNumber rcn3 = rcn1.add(rcn2);
		System.out.println("Add:" + "\n" + rcn3 + "\n");
		
		rcn3 = rcn1.sub(rcn2);
		System.out.println("Sub:" + "\n" + rcn3 + "\n");
		
		rcn3 = rcn1.mult(rcn2);
		System.out.println("Mult:" + "\n" + rcn3 + "\n");
	}
}
