/**
* @author Eldar Mingachev
* 11-601
* 004, 001
*/

public class Student {
	private String name;
	private String group;
	
	public Student() {
	}

	public Student(String name, String group) {
		this.name = name;
		this.group = group;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public String getGroup() {
		return group;
	}

}