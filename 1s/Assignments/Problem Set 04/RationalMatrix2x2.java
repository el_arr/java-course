/**
* @author Eldar Mingachev
* 11-601
* 004, 008
*/

package ru.kpfu.itis.el_arr.util;

public class RationalMatrix2x2 {
	private RationalFraction [][] matrix = new RationalFraction[2][2];
	
	public RationalMatrix2x2() {
		RationalFraction a = new RationalFraction();
		matrix[0][0] = a;
		matrix[0][1] = a;
		matrix[1][0] = a;
		matrix[1][1] = a;
	}

	public RationalMatrix2x2(RationalFraction a) {
		matrix[0][0] = a;
		matrix[0][1] = a;
		matrix[1][0] = a;
		matrix[1][1] = a;
	}
	
	public RationalMatrix2x2(RationalFraction a, RationalFraction b, RationalFraction c, RationalFraction d) {
		matrix[0][0] = a;
		matrix[0][1] = b;
		matrix[1][0] = c;
		matrix[1][1] = d;
	}

	public void setE(int i, int j, RationalFraction fr) {
		this.matrix[i][j] = fr;
	}

	public RationalFraction getE(int i,int j) {
		return this.matrix[i][j];
	}

	public RationalMatrix2x2 add(RationalMatrix2x2 rv) {
		RationalMatrix2x2 result = new RationalMatrix2x2();
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				result.matrix[i][j] = this.matrix[i][j].add(rv.matrix[i][j]);
			}
		}
		return result;
	}
	
	public RationalMatrix2x2 mult(RationalMatrix2x2 m) {
		RationalMatrix2x2 result = new RationalMatrix2x2();
		RationalFraction c = new RationalFraction();
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				c.setN(0);
				c.setD(1);
				for (int k = 0; k < matrix[i].length; k++) {
					c.add2(matrix[i][k].mult(m.matrix[k][j]));
				}
				result.matrix[i][j] = c;
			}
		}
		return result;
	}

	public double det() {
		return matrix[0][0].value() * matrix[1][1].value() - matrix[0][1].value() * matrix[1][0].value();
	}

	public RationalVector2D multVector(RationalVector2D rv) {
		RationalVector2D result = new RationalVector2D();
		RationalFraction [][] matrix = this.matrix;
		RationalFraction c = new RationalFraction();
		RationalFraction t = new RationalFraction();
		for (int i = 0; i < matrix.length; i++) {
			c.setN(0);
			c.setD(1);
			for (int j = 0; j < matrix[i].length; j++) {
				if (j % 2 == 0) {
					t = rv.getX();
				}
				else 
					if (j % 2 == 1) {
						t = rv.getY();
					}
				c.add2(matrix[i][j].mult(t));
			}
			if (i % 2 == 0) {
				result.setX(c.getN(),c.getD());
			}
			else 
				if (i % 2 == 1) {
					result.setY(c.getN(),c.getD());
				} 
		}
		return result;
	}


	public String toString() {
		String str = "";
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				str += matrix[i][j] + " ";
			}
			str += "\n";
		}
		return str;
	}

	public static void main() {
		RationalFraction a11 = new RationalFraction(1,2);
		RationalFraction a12 = new RationalFraction(3,4);
		RationalFraction a21 = new RationalFraction(5,6);
		RationalFraction a22 = new RationalFraction(7,8);
		RationalMatrix2x2 rm1 = new RationalMatrix2x2(a11,a12,a21,a22);
		System.out.println("RationalMatrix1: " + "\n" + rm1);

		RationalFraction a = new RationalFraction(1,2);
		RationalMatrix2x2 rm2 = new RationalMatrix2x2(a);
		System.out.println("\n" + "RationalMatrix2: " + "\n" + rm2);

		RationalMatrix2x2 rm3 = rm1.add(rm2);
		System.out.println("\n" + "rm1 add rm2:" + "\n" + rm3);
		
		rm3 = rm1.mult(rm2);
		System.out.println("\n" + "rm1 mult rm2: " + "\n" + rm3);
		
		System.out.println("\n" + "det rm3: " + "\n" + rm3.det());
		
		RationalFraction x = new RationalFraction(1,5);
		RationalFraction y = new RationalFraction(1,5);
		RationalVector2D rv1 = new RationalVector2D(x,y);
		RationalVector2D rv2 = rm1.multVector(rv1);
		System.out.println("m1 multVector rv1(1/5;1/5):" + "\n" + rv2 + "\n");
	}
}