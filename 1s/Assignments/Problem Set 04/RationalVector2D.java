/**
* @author Eldar Mingachev
* 11-601
* 004, 006
*/

package ru.kpfu.itis.el_arr.util;

public class RationalVector2D {
	private RationalFraction x = new RationalFraction();
	private RationalFraction y = new RationalFraction();

	public RationalVector2D() {
	}

	public RationalVector2D(RationalFraction x, RationalFraction y) {
		this.x = x;
		this.y = y;
	}
	
	public void setX(int nx, int dx) {
		this.x.setN(nx);
		this.x.setD(dx);
	}

	public RationalFraction getX() {
		return this.x;
	}

	public void setY(int ny, int dy) {
		this.y.setN(ny);
		this.y.setD(dy);		
	}

	public RationalFraction getY() {
		return this.y;
	}	

	public RationalVector2D add(RationalVector2D rv) {
		RationalVector2D result = new RationalVector2D();
		result.x = this.x.add(rv.x);
		result.y = this.y.add(rv.y);
		return result;
	}

	public double length() {
		return Math.sqrt(((this.x.value()) * (this.x.value())) + (this.y.value() * this.y.value()));
	}

	public RationalFraction scalarProduct(RationalVector2D rv) {
		return (this.x.mult(rv.x)).add(this.y.mult(rv.y));
	}

	public boolean equals(RationalVector2D rv) {
		if ((this.x.equals(rv.x)) && (this.y.equals(rv.y))) {
			return true;
		}
		else 
			return false;
	}

	public String toString() {
		return "<" + x + ", " + y + ">";
	}

	public static void main() {
		RationalFraction x1 = new RationalFraction(1,2);
		RationalFraction y1 = new RationalFraction(3,4);
		RationalVector2D rv1 = new RationalVector2D(x1,y1);
		System.out.println("RV1: " + "\n" + rv1);

		RationalFraction x2 = new RationalFraction(5,6);
		RationalFraction y2 = new RationalFraction(7,8);
		RationalVector2D rv2 = new RationalVector2D(x2,y2);
		System.out.println("\n" + "RV2: " + "\n" + rv2);

		RationalVector2D rv3 = rv1.add(rv2);
		System.out.println("\n" + "RV3: " + "\n" + rv3);

		double t = rv3.length();
		System.out.println("\n" + "Length RV3: " + "\n" + t);

		RationalFraction fr = rv1.scalarProduct(rv2);
		System.out.println("\n" + "ScalarProduct: " + "\n" + fr);

		for (int i = 0; i < 2; i++) {
			if (i == 1) {
				rv1.setX(1,2);
				rv1.setY(2,5);
				rv2.setX(1,2);
				rv2.setY(2,5);
				System.out.println("For x1 = x2 = 1/2, y1 = y2 = 2/5 :");
			}
		
			if (rv1.equals(rv2)) {
				System.out.println("v1 equals v2");
			}
			else
				System.out.println("\n" + "v1 doesn't equal v2" + "\n");
		}
	}
}