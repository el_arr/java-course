/**
* @author Eldar Mingachev
* 11-601
* 004, 001
*/

public class TestClass {
	public static void main(String[] args) {
		Student s1 = new Student();
		s1.setName("Student");
		s1.setGroup("Group");
		Teacher t1 = new Teacher();
		t1.setName("Teacher");
		t1.setSubject("Subject");
		System.out.println("Студент " + s1.getName() + "\n" + "Группа № " + s1.getGroup());
		t1.classifyStudent(s1);
		
		System.out.println("\n" + "Vector2D:");
		Vector2D.main();
		
		System.out.println("\n" + "RationalFraction:");
		RationalFraction.main();
		
		System.out.println("\n" + "ComplexNumber:");
		ComplexNumber.main(); 
		
		System.out.println("\n" + "Matrix2x2:");
		Matrix2x2.main();
		
		System.out.println("\n" + "RationalVector2D:");
		RationalVector2D.main();
		
		System.out.println("\n" + "ComplexVector2D:");
		ComplexVector2D.main();

		System.out.println("\n" + "RationalMatrix2x2:");
		RationalMatrix2x2.main();

		System.out.println("\n" + "ComplexMatrix2x2:");
		ComplexMatrix2x2.main();

		System.out.println("\n" + "RationalComplexNumber:");
		RationalComplexNumber.main();

		System.out.println("\n" + "RationalComplexVector2D:");
		RationalComplexVector2D.main();
		
		System.out.println("\n" + "RationalComplexMatrix2x2:");
		RationalComplexMatrix2x2.main();
	}
}