import java.util.Scanner;

public class Task13{
	public static void main(String [] args){

		Scanner in = new Scanner(System.in);

		System.out.println("Введите N");
		int n =  in.nextInt();
		System.out.println("Введите k");
		int k =  in.nextInt();
		int c = 0;
		n = Math.abs(n);
		while (n>0){
			if (k==(n%10)){
				c++;
			}
			n = n/10;
		}
		System.out.println("Count = " + c);
	}
}

/**
* @author Eldar Mingachev
* 11-601
* 000, 013
*/	