import java.util.Scanner;

public class Task09{
	public static void main(String [] args){

		Scanner in = new Scanner(System.in);

		System.out.println("Введите n (2<=n<=9)");
		int n =  in.nextInt();
		int res;
		for (int i = 2; i <=10; i++) {
			res = n*i;
			System.out.println(n + "*" + i + " = " + res);
		}

	}
}

/**
* @author Eldar Mingachev
* 11-601
* 000, 009
*/	