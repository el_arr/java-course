public class Task02 {
	public static void main(String [] args) {
	
		double x = Double.parseDouble(args[0]);
	
		if (x < -3.5 || x > 0) {
			System.out.println("Принадлежит");
		}
		else {
			System.out.println("Не принадлежит");
		}
	
	}
}

/**
* @author Eldar Mingachev
* 11-601
* 000, 002
*/	