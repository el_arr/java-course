import java.util.Scanner;

public class Task15 {
	public static void main(String [] args) {

		Scanner in = new Scanner(System.in);
		
		System.out.println("Введите N");
		int n = in.nextInt();
		System.out.println("Введите k");
		int k = in.nextInt();
		int i, d, deg = 1;
		int n1 = 0;
		while (n > 0) {
			d = n % 10;
			if (d != k) {
				n1 += d * deg;
				deg *= 10;
			}
			n = n / 10;
		}
		System.out.println("N1 = " + n1);
		
	}	
}

/**
* @author Eldar Mingachev
* 11-601
* 000, 015
*/