import java.util.Scanner;

public class Task11{
	public static void main(String [] args){

		Scanner in = new Scanner(System.in);

		System.out.println("Введите n");
		int n =  in.nextInt();
		System.out.println("Введите способ представления (1 - верт., 2 - гориз.) ");
		int choice =  in.nextInt();
		if (choice == 1) {
			for (int i = 1; i <=n; i++) {
				System.out.print(i + " ");
				if (i<10) {
					System.out.print(" ");
				}
				System.out.print(i*i);
				System.out.println();
			}
		}
		else 
			if (choice == 2) {
				for (int i = 1; i <=n; i++) {
					if (i*i > 10) {
						System.out.print(" ");
					}
					System.out.print(i + " ");
				}
				System.out.println();
				for (int i = 1; i <=n; i++) {
					System.out.print(i*i + " ");
				}
			}
	}
}

/**
* @author Eldar Mingachev
* 11-601
* 000, 011
*/	