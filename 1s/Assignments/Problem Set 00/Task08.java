import java.util.Scanner;

public class Task08{
	public static void main(String [] args){
		
		Scanner in = new Scanner(System.in);
		
		double sum = 0;
		System.out.println("Введите чётное N");
		int n =  in.nextInt();
		for (double i = 2; i <= n; i = i + 2) {
			sum += ((i-1)*(i-1))/(i*i);
		}
		System.out.println("Sum = " + sum);
	}
}

/**
* @author Eldar Mingachev
* 11-601
* 000, 008
*/	