import java.util.Scanner;

public class Task06{
	public static void main(String [] args){
		
		Scanner in = new Scanner(System.in);

		System.out.println("Введите a b c");
		double a =  in.nextInt();
		double b =  in.nextInt();
		double c =  in.nextInt();
		double d = b*b - 4*a*c;
		double x1 = ((-b)+Math.sqrt(d))/(2*a);
		double x2 = ((-b)-Math.sqrt(d))/(2*a);
		System.out.println("x1 = " + x1 + " x2 = " + x2);

	}
}

/**
* @author Eldar Mingachev
* 11-601
* 000, 006
*/	