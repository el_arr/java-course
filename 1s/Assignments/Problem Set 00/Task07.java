import java.util.Scanner;

public class Task07 {
	public static void main(String [] args) {
		
		Scanner in = new Scanner(System.in);

		System.out.println("Введите x n");
		int x = in.nextInt();
		int n = in.nextInt();
		int res = 1;
		for (int i = 0; i < n; i++) {
			res *= x;
		}
		System.out.println(x + "^" + n + " = " + res);
		
	}
}

/**
* @author Eldar Mingachev
* 11-601
* 000, 007
*/	