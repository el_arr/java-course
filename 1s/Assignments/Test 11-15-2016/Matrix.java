/**
* @author Eldar Mingachev
* 11-601
* KR2, 002
*/

import java.util.Scanner;

public class Matrix {

	public static void main(String[] args) {
		
		Scanner in = new Scanner(System.in);
		System.out.println("Введите m и n");
		int m = in.nextInt();
		int n = in.nextInt();
		int []arr1 = new int[m];
		int []arr2 = new int[n]; 
		
		System.out.println("Введите массивы 1 и 2");
		for (int i = 0; i < m; i++) {
			arr1[i] = in.nextInt();		
		}
		for (int j = 0; j < n; j++) {
			arr2[j] = in.nextInt();
		}

	}

}