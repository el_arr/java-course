package assignments;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Task01 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        Pattern p = Pattern.compile("(06\\\\/03\\\\/1237 ((1[2-9])|(2[0-3])):[0-5][0-9])|((0[7-9]|([12][0-9])|3[01]\\\\/03)|(([0-2][0-9])|(3[01]\\\\/((0[4-9])|(1[0-2])))\\\\/1237 ([01][0-9])|(2[0-3]):[0-5][0-9]))|(([0-2][0-9])|(3[01])\\\\/((0[1-9])|(1[0-2]))\\\\/((1[2-8][0-9]{2})|(19[0-6][0-9])|(197[0-7]) ([01][0-9])|(2[0-3]):[0-5][0-9]))|((([0-2][0-9])|(3[01])\\\\/01)|(([01][0-9])|(2[0-7]))\\\\/02)\\\\/1978 ([01][0-9]:[0-5][0-9])|2(0:[0-5][0-9])|(1:([0-2][0-9])|(3[0-5]))");
        System.out.println("Введите строку");
        String s = in.nextLine();
        Matcher m = p.matcher(s);
        if (m.matches()) {
            System.out.println("Подходит");
        } else {
            System.out.println("Не подходит");
        }
    }
}