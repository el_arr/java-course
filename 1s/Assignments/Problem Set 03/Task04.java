package assignments;

import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Task04 {

    public static void main(String[] args) {
        final int N = 10;
        int count = 0;
        int c = 0;
        Random r = new Random(0);
        String[] a = new String[N];
        String temp;
        Matcher m;
        Pattern p = Pattern.compile("\\d*[02468]$");
        while (a[N - 1] == null) {
            temp = String.valueOf(Math.abs(r.nextInt()));
            m = p.matcher(temp);
            if (m.matches()) {
                a[count] = temp;
                count++;
            }
            c++;
        }
        System.out.println("Числа, удовлетворяющие регулярному выражению:");
        for (String s : a) {
            System.out.println(s);
        }
        System.out.println("Общее количество чисел = " + c);
    }
}
