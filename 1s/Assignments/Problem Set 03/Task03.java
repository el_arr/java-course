package assignments;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Task03 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Введите кол-во строк");
        int n = in.nextInt() + 1;
        String[] a = new String[n];
        int[] b = new int[n];
        int c = 0;
        Matcher m;
        Pattern p = Pattern.compile("(0+)|(1+)|(01)+0?|(10)+1?");
        for (int i = 0; i < n; i++) {
            a[i] = in.nextLine();
            m = p.matcher(a[i]);
            if (m.matches()) {
                b[c] = i;
                c++;
            }
        }
        System.out.println("Номера строк, подходящих регулярному выражению:");
        for (int i = 0; i < c; i++) {
            System.out.println(b[i]);
        }
    }
}