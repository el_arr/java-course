/**
* @author Eldar Mingachev
* 11-601
* 001, 033б
*/	

import java.util.Scanner;

public class PS1_Task033b{

	public static void main(String [] args) {

		Scanner in = new Scanner(System.in);
		System.out.println("Введите X, Y");
		double x =  in.nextDouble();
		double y =  in.nextDouble();
		double res;
		if (x<y) {
			res = x;
		}
		else 
			res = y;
		System.out.println("Min = " + res);
	
	}
	
}