/**
* @author Eldar Mingachev
* 11-601
* 001, 059к
*/	

import java.util.Scanner;

public class PS1_Task059k{

	public static void main(String [] args) {

		Scanner in = new Scanner(System.in);
		System.out.println("Введите координаты точки (x;y)");
		double x =  in.nextDouble();
		double y =  in.nextDouble();
		if (((x >= -1) && (y >= -1*x) && (y >= x) && (x <= 1)) || (y >= 1)){
			System.out.println("Принадлежит");
		}
		else {
			System.out.println("Не принадлежит");
		}
		
	}
	
}