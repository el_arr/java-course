/**
* @author Eldar Mingachev
* 11-601
* 001, 083а
*/	

import java.util.Scanner;

public class PS1_Task083a{

	public static void main(String [] args) {

		Scanner in = new Scanner(System.in);
		System.out.println("Введите a");
		double a =  in.nextDouble();
		double i = 0;
		double cur = 0;	
		do {
			i++;
			cur += 1/i;
		}
		while (cur <= a);
		System.out.println("Результат = " + cur);
	
	}

}