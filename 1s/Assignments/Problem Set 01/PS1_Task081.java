/**
* @author Eldar Mingachev
* 11-601
* 001, 081
*/	

import java.util.Scanner;

public class PS1_Task081{

	public static void main(String [] args) {

		Scanner in = new Scanner(System.in);
		System.out.println("Введите x, a, n");
		double x = in.nextDouble();
		double a = in.nextDouble();
		int n = in.nextInt();
		double res = x+a;
		for (int i = 0; i < n; i++){
			res = res * res;
			res += a;
		}
		System.out.println("Результат = " + res);
		
	}

}