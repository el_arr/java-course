/**
* @author Eldar Mingachev
* 11-601
* 001, 060д
*/	

import java.util.Scanner;

public class PS1_Task060e{

	public static void main(String [] args) {

		Scanner in = new Scanner(System.in);
		System.out.println("Введите координаты точки (x;y)");
		double x =  in.nextDouble();
		double y =  in.nextDouble();
		double u = x - y;
		if ((y <= Math.exp(-1*x)) && (y <= Math.exp(x)) && (y >= (x*x))) {
			u = x + y;
		}
		System.out.println("U = " + u);
		
	}
	
}