/**
* @author Eldar Mingachev
* 11-601
* 001, 060а
*/	

import java.util.Scanner;

public class PS1_Task060a{

	public static void main(String [] args) {

		Scanner in = new Scanner(System.in);
		System.out.println("Введите координаты точки (x;y)");
		double x =  in.nextDouble();
		double y =  in.nextDouble();
		double u = x;
		if ((y >= 0) && (x * x + y * y >= 1) && (x * x + y * y <= 4)) {
			u = 0;
		}
		System.out.println("U = " + u);
		
	}
	
}
		