/**
* @author Eldar Mingachev
* 11-601
* 001, 060в
*/	

import java.util.Scanner;

public class PS1_Task060c{

	public static void main(String [] args) {

		Scanner in = new Scanner(System.in);
		System.out.println("Введите координаты точки (x;y)");
		double x =  in.nextDouble();
		double y =  in.nextDouble();
		double u = x*y + 7;
		if (((x* x + (y - 1) * (y - 1)) <= 1) && (y <= (1 - x*x))) {
			u = x - y;
		}
		System.out.println("U = " + u);
		
	}
	
}