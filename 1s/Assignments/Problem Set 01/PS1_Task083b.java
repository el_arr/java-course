/**
* @author Eldar Mingachev
* 11-601
* 001, 083б
*/	

import java.util.Scanner;

public class PS1_Task083b{

	public static void main(String [] args) {

		Scanner in = new Scanner(System.in);
		System.out.println("Введите a");
		double a =  in.nextDouble();
		double i = 0;
		double res = 0;
		do {
			i++;
			res += 1/i;
		}
		while (res <= a);
		System.out.println("N = " + i);
	
	}

}