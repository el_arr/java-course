/**
* @author Eldar Mingachev
* 11-601
* 001, 031и
*/	

import java.util.Scanner;

public class PS1_Task031i{

	public static void main(String [] args) {

		Scanner in = new Scanner(System.in);
		System.out.println("Введите A");
		double x =  in.nextDouble();
		double x1=x*x;
		double x2=x1*x1;
		double x3=x2*x2;
		double x4=x3*x3;
		double res = x4*x2*x;
		//Шесть умножений
		System.out.println("A^21 = " + res);
		
	}
	
}