/**
* @author Eldar Mingachev
* 11-601
* 001, 059з
*/	

import java.util.Scanner;

public class PS1_Task059h{

	public static void main(String [] args) {

		Scanner in = new Scanner(System.in);
		System.out.println("Введите координаты точки (x;y)");
		double x =  in.nextDouble();
		double y =  in.nextDouble();
		if ((x >= -1) && (x <= 1) && (y >= -2) && ((y <= x) || (y <= -x))){
			System.out.println("Принадлежит");
		}
		else
			System.out.println("Не принадлежит");
	
	}
	
}