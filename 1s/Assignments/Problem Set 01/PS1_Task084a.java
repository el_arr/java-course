/**
* @author Eldar Mingachev
* 11-601
* 001, 084а
*/	

import java.util.Scanner;

public class PS1_Task084a{

	public static void main(String [] args) {

		Scanner in = new Scanner(System.in);
		System.out.println("Введите N и X");
		int n =  in.nextInt();
		double x =  in.nextDouble();
		double cur = 1;
		double res = 0;
		for (int i = 0; i < n; i++) {
			cur *= Math.sin(x);
			res += cur;
		} 
		System.out.println("sin(x) + sin^2(x) + .. + sin^n(x)  = " + res);
	
	}

}