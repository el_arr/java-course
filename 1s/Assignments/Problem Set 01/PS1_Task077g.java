/**
* @author Eldar Mingachev
* 11-601
* 001, 077ж
*/	

import java.util.Scanner;

public class PS1_Task077g{
	public static void main(String [] args){
		Scanner in = new Scanner(System.in);
		System.out.println("Введите n");
		double n =  in.nextInt();
		double cur = 0;
		double res = 0;
		for (double i = 0; i < n; i++){
			cur = 3*(n-i) + res;
			res = Math.sqrt(cur);
		}
		System.out.println("Результат = " + res);
	}
}