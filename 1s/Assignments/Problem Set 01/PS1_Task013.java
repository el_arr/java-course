import java.util.Scanner;

public class PS1_Task013{
	
	public static void main(String [] args){
		
		Scanner in = new Scanner(System.in);
		
		final double g = 9.8;
		
		System.out.println("Введите l");
		double l = in.nextDouble();
		double t = 2*Math.PI*Math.sqrt(l/g);
		System.out.println("T = " + t);
		
	}
	
}

/**
* @author Eldar Mingachev
* 11-601
* 001, 013
*/	