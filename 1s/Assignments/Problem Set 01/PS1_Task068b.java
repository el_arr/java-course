/**
* @author Eldar Mingachev
* 11-601
* 001, 068б
*/	

import java.util.Scanner;

public class PS1_Task068b{
	public static void main(String [] args){
		Scanner in = new Scanner(System.in);
		System.out.println("Введите n");
		int n =  in.nextInt();
		int count = 0;
		int c = 0;
		int dt = n;
		boolean threeCh = false;
		boolean threeMore = false;
		while (dt > 0) {
			dt = dt/10;
			c++;
		}
		int [] d = new int[c];
		for (int i = 0; i < c; i++){
			d[i] = n%10;
			n = n/10;
		}
		for (int i = 0; ((i < c)&&(!threeMore)); i++){
			for (int j = 0; j < c; j++){
				if (d[j] == d[i]) {
					count++;
				}
			}
			if (count == 3) {
				threeCh = true;
			}
			else 
				if (count > 3) {
					threeMore = true;
			}
			count = 0;
		}
		if (threeMore){
			System.out.println("Не содержит");
		}
		else
			if (threeCh){
				System.out.println("Содержит");
			}
			else
				System.out.println("Не содержит");
	}
}