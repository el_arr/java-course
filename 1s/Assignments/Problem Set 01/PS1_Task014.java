import java.util.Scanner;

public class PS1_Task014{

	public static void main(String [] args){
		
		Scanner in = new Scanner(System.in);
		
		final double G = 6.67408 * Math.pow(10,-11);
		System.out.println("Введите m1 m2");
		double m1 =  in.nextDouble();
		double m2 =  in.nextDouble();
		System.out.println("Введите r");
		double r =  in.nextDouble();
		double f =  (G * m1 * m2)/(r*r);
		System.out.println("F = " + f);
	
	}
	
}

/**
* @author Eldar Mingachev
* 11-601
* 001, 014
*/	