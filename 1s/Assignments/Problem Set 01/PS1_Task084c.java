/**
* @author Eldar Mingachev
* 11-601
* 001, 084в
*/	

import java.util.Scanner;

public class PS1_Task084c{

	public static void main(String [] args) {

		Scanner in = new Scanner(System.in);
		System.out.println("Введите N и X");
		int n =  in.nextInt();
		double x =  in.nextDouble();
		double cur = x;
		double res = 0;
		for (int i = 0; i < n; i++) {
			cur = Math.sin(cur);
			res += cur;
		}
		System.out.println("sin(x) + sin(sin(x)) + .. + sin(sin(..(sin(x))))  = " + res);
	
	}

}