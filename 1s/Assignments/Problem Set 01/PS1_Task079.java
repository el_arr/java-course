/**
* @author Eldar Mingachev
* 11-601
* 001, 079
*/	

public class PS1_Task079{

	public static void main(String [] args){

		double i = 0.1;
		double cur = 0;
		double res = 1;
		while (i<=10) {
			cur = 1 + Math.sin(i);
			res *= cur;
			i += 0.1;
		}
		System.out.println("Результат = " + res);
	
	}

}