/**
* @author Eldar Mingachev
* 11-601
* 001, 077д
*/	

import java.util.Scanner;

public class PS1_Task077e{
	public static void main(String [] args){
		Scanner in = new Scanner(System.in);
		System.out.println("Введите n");
		double n =  in.nextInt();
		double res = 2;
		for (double i = 0; i < n; i++){
			res = 2 + Math.sqrt(res);
		}
		System.out.println("Результат = " + res);
	}
}