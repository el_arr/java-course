/**
* @author Eldar Mingachev
* 11-601
* 001, 077а
*/	

import java.util.Scanner;

public class PS1_Task077a{
	public static void main(String [] args){
		Scanner in = new Scanner(System.in);
		System.out.println("Введите n");
		int n =  in.nextInt();
		int res = 1;
		for (int i = 0; i < n; i++){
			res *= 2;
		}
		System.out.println("2^n = " + res);
	}
}