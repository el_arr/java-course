/**
* @author Eldar Mingachev
* 11-601
* 001, 011в
*/	

import java.util.Scanner;

public class PS1_Task011c{

	public static void main(String [] args) {

		Scanner in = new Scanner(System.in);
		System.out.println("Введите X, Y, Z");
		double x =  in.nextDouble();
		double y =  in.nextDouble();
		double z =  in.nextDouble();
		double a = ((1 + y)*(x + (y/(x*x + 4))))/(Math.exp((-1*x)-2) + 1/(x*x + 4));
		double b = (1+Math.cos(y-2))/((x*x*x*x/2)+(Math.sin(z)*Math.sin(z)));
		System.out.println("a = " + a);
		System.out.println("b = " + b);
		
	}

}