import java.util.Scanner;

public class PS1_Task016{

	public static void main(String [] args){
		
		Scanner in = new Scanner(System.in);
		
		System.out.println("Введите L");
		double l =  in.nextDouble();
		double s = (l * l) / (4 * Math.PI);
		System.out.println("S = " + s);
		
	}
	
}

/**
* @author Eldar Mingachev
* 11-601
* 001, 016
*/	