/**
* @author Eldar Mingachev
* 11-601
* 001, 114ж
*/	

public class PS1_Task114g{

	public static void main(String [] args) {

		double res = 1;
		for (double i = 2; i <=100; i++) {
			res *= ((i + 1) / (i + 2));
		}
		System.out.println("Результат = " + res);
		
	}
	
}