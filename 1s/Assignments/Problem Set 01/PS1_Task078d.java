/**
* @author Eldar Mingachev
* 11-601
* 001, 078г
*/	

import java.util.Scanner;

public class PS1_Task078d{
	public static void main(String [] args){
		Scanner in = new Scanner(System.in);
		System.out.println("Введите a");
		int a =  in.nextInt();
		System.out.println("Введите n");
		int n =  in.nextInt();
		int powC = 0;
		int powN = 1;
		double t = 1;
		double res = 0;
		for (int i = 0; i <= n; i++){
			for (int j = powC; j < powN; j++){
				t *= a;
			}
			res += 1/t;
			powC = powN;
			powN *= 2;
		}
		System.out.println("Результат = " + res);
	}
}