/**
* @author Eldar Mingachev
* 11-601
* 001, 067d
*/	

import java.util.Scanner;

public class PS1_Task067d{
	public static void main(String [] args){
		Scanner in = new Scanner(System.in);
		System.out.println("Введите n (10 <= n <= 100)");
		int n =  in.nextInt();
		double p = 0;
		for (int i = 0; i < 2; i++){
			p = n%10;
			n = n/10;
		}
		System.out.println("p = " + p);	
	}
}