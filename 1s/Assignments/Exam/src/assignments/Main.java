package assignments;

public class Main {

    public static void main(String[] args) {
        RationalFrac r1 = new RationalFrac(1, 2);
        RationalFrac r2 = new RationalFrac(3, 1, 4);
        System.out.println("Сумма: " + r1.sum(r2));
        System.out.println("\n" + "Произведение: " + r1.mult(r2) + "\n");
        r1.sout();
        r2.sout();
    }
}
