public class Task01{

	public static void main(String [] args) {
	
		int n = Integer.parseInt(args[0]);
		int [] a = new int[n];
		int i, max;
		
		for (i = 0; i < n; i++) {
			a[i] = Integer.parseInt(args[i+1]);
		}
		max = a[0];
		for (i = 1; i < n; i++) {
			if (max < a[i]) {
				max = a[i];
			}
		}	
		System.out.println(max);
	
	}
	
}