public class Task03{

	public static void main(String [] args) {
	
		int n = Integer.parseInt(args[0]);
		int [] a = new int[n];
		int i, max, p;
		
		for (i = 0; i < n; i++) {
			a[i] = Integer.parseInt(args[i+1]);
		}
		System.out.println("");
		for (i = 0; i < n/2; i++) {
			p = a[i];
			a[i] = a[n - i - 1];
			a[n - i - 1] = p;
		}
		for (i = 0; i < n; i++) {
			System.out.print(a[i]);
			System.out.print(" ");
		}
		System.out.print("");
	
	}
	
}