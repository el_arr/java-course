public class Task04{

	public static void main(String [] args) {
	
		int n = Integer.parseInt(args[0]);
		int k = Integer.parseInt(args[1]);
		int [] a = new int[n];
		int i, max, p;
		int j = 0;
		
		for (i = 0; i < n; i++) {
			a[i] = Integer.parseInt(args[i+2]);
		}
		System.out.println("");
		for (i = 0; i < n; i++) {
			System.out.print(a[i]);
			System.out.print(" ");
		}
		System.out.println("");
		System.out.println("");
		for (i = 0; i < n/2; i++) {
			p = a[i];
			a[i] = a[n - i - 1];
			a[n - i - 1] = p;
		}
		int d = n - k;
		for (i = 0; i < d/2; i++) {
			p = a[i];
			a[i] = a[d - i - 1];
			a[d - i - 1] = p;
		}
		for (i = d; i <= d + k/2; i++) {
			p = a[i];
			a[i] = a[n - j - 1];
			a[n - j - 1] = p;
			j++;
		}
		for (i = 0; i < n; i++) {
			System.out.print(a[i]);
			System.out.print(" ");
		}
		System.out.println("");
	
	}
	
}