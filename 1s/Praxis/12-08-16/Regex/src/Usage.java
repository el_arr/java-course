import java.util.Scanner;

public class Usage {

    public static void main(String[] args) {

        final int N = 8;
        Scanner in = new Scanner(System.in);
        System.out.println("Введите номер задания");
        int i = in.nextInt();
        System.out.println("Введите строку");
        String checkable = in.nextLine();
        if (matches(i, checkable)) {
            System.out.println("Строка " + i + " подходит заданным условиям");
        } else
            System.out.println("Ввод некорректен");
    }

    public static boolean matches(int i, String s) {
        switch (i) {
            case 0:
                if (s.matches("^[A-z].*[0-9]$")) {
                    return true;
                }
                break;
            case 1:
                if (s.matches("([A-z]|[0-9])+@[A-z]+.[a-z]+")) {
                    return true;
                }
            case 2:
                if (s.matches("([0-9]{3}.){3}[0-9]{2}")) {
                    return true;
                }
            case 3:
                if (s.matches("^((\\+[0-9])|[0-9])[0-9]{10}")) {
                    return true;
                }
            case 4:
                if (s.matches("((\\w)+.)+.[a-z]+")) {
                    return true;
                }
            case 5:
                if (s.matches("([A-Z]([a-z]|[0-9])\\W*)+")) {
                    return true;
                }
        }
        return false;
    }
}
