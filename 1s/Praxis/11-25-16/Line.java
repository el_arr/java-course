import java.util.Scanner;

public class Line {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		final int SIZE = 30;
		System.out.println("Введите k, b");
		double k = in.nextDouble();
		double b = in.nextDouble();
		for (int j = 0; j <= SIZE; j++) {
			for (int i = 0; i <= SIZE; i++) {
				if ((j) == (int) (k * (SIZE - i) + (SIZE - b + 2))) {
					System.out.print("/");
				}
				else {
					System.out.print(" ");
				}
			}
			System.out.println();
		}
	}
}