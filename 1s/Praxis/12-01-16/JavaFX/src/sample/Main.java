package sample;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Group root = new Group();
        TextField tf = new TextField();
        Button bt = new Button("КНОПОЩКА");
        bt.setLayoutX(100);
        bt.setLayoutY(100);
        /*
        bt.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                tf.setText("" + Math.random());
            }
        });
        */
        bt.setOnAction(event -> {
            tf.setText("" + Math.random());
        });
        root.getChildren().add(tf);
        root.getChildren().add(bt);
        primaryStage.setTitle("Greetings");
        primaryStage.setScene(new Scene(root, 400, 400));
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
