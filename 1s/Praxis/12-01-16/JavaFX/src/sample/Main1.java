package sample;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;

public class Main1 extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Group root = new Group();

        Circle circle = new Circle(30, 30, 30);
        circle.setStyle("-fx-fill: lightseagreen");
        root.getChildren().add(circle);
        root.setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.RIGHT) {
                circle.setCenterX(circle.getCenterX() + 10);
            }
        });

        primaryStage.setTitle("Greetings");
        primaryStage.setScene(new Scene(root, 400, 400));
        primaryStage.show();

        primaryStage.getScene().getRoot().requestFocus();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
