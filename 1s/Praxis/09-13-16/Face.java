public class Face{

	public static void main(String [] args) {
	
		double r = Double.parseDouble(args[0]);
		
		double r1 = Double.parseDouble(args[1]);
		
		double r2 = Double.parseDouble(args[2]);
		
		for (double i = 0; i <= 2*r; i++) {
			
			for (double j = 0; j <= 2*r; j++) {
				
				if (((i - r)*(i - r) <= (r*r - (j - r)*(j - r))) && 
					((j - r)*(j - r) <= (r*r - (i - r)*(i - r)))) {
					
					if (((i - 1.5 * r)*(i - 1.5 * r) <= (r2 * r2 - (j - r)*(j - r))) &&
						((j - r)*(j - r) <= (r2 * r2 - (i - 1.5 * r)*(i - 1.5 * r)))) {
					
						System.out.print("  ");
					
					}
					
					else 
						if (((i - r / 2 - 2)*(i - r / 2 - 2) <= (r1 * r1 - (j - r / 2)*(j - r / 2))) &&
							((j - r / 2)*(j - r / 2) <= (r1 * r1 - (i - r / 2 - 2)*(i - r / 2 - 2)))) {
					
							System.out.print("# ");
					
						}
					
						else
							if (((i - r / 2 - 2)*(i - r / 2 - 2) <= (r1 * r1 - (j - 1.5 * r)*(j - 1.5 * r))) &&
								((j - 1.5 * r)*(j - 1.5 * r) <= (r1 * r1 - (i - r / 2 - 2)*(i - r / 2 - 2)))) {
							
								System.out.print("# ");
						
							}
						
							else {
						
								System.out.print("* ");
						
						}
				}
				
				else{
					
					System.out.print("  ");
					
				}
				
			}
		
			System.out.println("");
		
		}
		
	}
}