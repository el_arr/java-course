public class Circle{

	public static void main(String [] args) {
	
		double r = Double.parseDouble(args[0]);
		
		double d = 2 * r;
		
		for (double i = 0; i <= d; i++) {
			
			for (double j = 0; j <= d; j++) {
				
				if (((i - r)*(i - r) <= (r*r - (j - r)*(j - r))) &&
					((j - r)*(j - r) <= (r*r - (i - r)*(i - r)))) {

					System.out.print("* ");
					
				}
				
				else{
					
					System.out.print("  ");
					
				}
				
			}
		
			System.out.println("");
		
		}
		
	}
}