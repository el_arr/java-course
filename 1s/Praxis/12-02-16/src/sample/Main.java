package sample;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;

public class Main extends Application {
    private final static int WIDTH = 500;
    private final static int HEIGHT = 500;
    private Player player;
    private Scene root;

    @Override
    public void start(Stage primaryStage) throws Exception {
        Group root = new Group();
        player = new Player(WIDTH / 2, HEIGHT - 30, root);

        Enemy[] enemies = new Enemy[50];
        final int[] enemies_number = {0};
        new AnimationTimer() {
            long was = System.currentTimeMillis();

            @Override
            public void handle(long now) {
                for (int i = 0; i < enemies_number[0]; i++) {
                    enemies[i].updateY();
                }
                if (System.currentTimeMillis() - was > 1000) {
                    enemies[enemies_number[0]] = new Enemy(enemies, player, root);
                    enemies_number[0]++;
                    was = System.currentTimeMillis();
                }
            }
        }.start();

        root.setOnKeyPressed(event -> {
            switch (event.getCode()) {
                case RIGHT: {
                    player.updateX(1);
                    break;
                }
                case LEFT: {
                    player.updateX(-1);
                    break;
                }
                case UP: {
                    player.updateY(1);
                    break;
                }
                case DOWN: {
                    player.updateY(-1);
                    break;
                }
                /* case SPACE: {
                    Circle bullet = new Circle(player.getX(), player.getY(), 3);
                    new AnimationTimer() {
                        @Override
                        public void handle(long now) {
                            bullet.setCenterX(bullet.getCenterY() - 1);
                            if (bullet.getCenterY() == 0) {
                                this.stop();
                            }
                        }
                    }.start();
                } */
            }
        });

        primaryStage.setTitle("Game");
        primaryStage.setScene(new Scene(root, WIDTH, HEIGHT));
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
