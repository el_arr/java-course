package sample;

import javafx.scene.Group;

import java.util.ArrayList;
import java.util.Random;

public class Enemy {
    private final static int WIDTH = 500;
    private final static int HEIGHT = 500;
    private double x, y, v;
    private VisualEnemy visualEnemy;

    public Enemy(Enemy[] el, Player p, Group group) {
        Random r = new Random();
        boolean check = true;
        double x0 = -1;
        double y0 = -1;
        while (check) {
            x0 = r.nextInt(WIDTH);
            y0 = r.nextInt(HEIGHT / 2);
            check = false;
            for (int i = 0; ((i < el.length) && (!check)); i++) {
                if (((x0 == el[i].getX() + visualEnemy.getRADIUS()) && (y0 == el[i].getY())) ||
                        ((x0 == p.getX()) && (y0 == p.getY()))) {
                    check = true;
                }

            }
        }
        this.v = 1;
        this.x = x0;
        this.y = y0;
        this.visualEnemy = new VisualEnemy(group, this);
    }

    public void updateY() {
        this.y = this.y + v;
        visualEnemy.updateY();
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public VisualEnemy getVisualEnemy() {
        return visualEnemy;
    }
}
