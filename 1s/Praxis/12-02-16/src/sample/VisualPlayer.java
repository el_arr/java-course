package sample;

import javafx.scene.Group;
import javafx.scene.shape.Circle;

public class VisualPlayer {
    private final int RADIUS = 50;
    private Circle circle;
    private Player player;

    public VisualPlayer(Group root, Player p) {
        this.player = p;
        circle = new Circle(p.getX(), p.getY(), RADIUS);
        root.getChildren().add(circle);
    }

    public void updateX() {
        circle.setCenterX(player.getX());
    }

    public void updateY() {
        circle.setCenterY(player.getY());
    }
}
