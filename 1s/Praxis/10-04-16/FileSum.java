import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class FileSum {

	public static void main(String [] args) throws FileNotFoundException {
	
		Scanner sc = new Scanner (new File("Sum_input.txt"));
		
		int sum = 0;
		int cur;
		
		do {
			cur = sc.nextInt();
			sum += cur;
		}
		while  (sc.hasNextLine());
		System.out.println(sum);
		
	}
}