public class Demidovich42c{
	public static void main(String [] args) {
		
		double eps = Double.parseDouble(args[0]);
		double fact = 1;
		double xp = 1;
		double xn = 1;
		double n = 1;
		double i;
		
		do {
			n++;
			xp = xn;
			fact = fact*n;
			xn = 1 / fact;
		}
		while (Math.abs(xn - xp) > eps);
		System.out.println(Math.abs(xn - xp));
		
	}
}	