public class Demidovich49{
	public static void main(String [] args) {
	
		double eps = Double.parseDouble(args[0]);
		int n = 0;		
		double f = 1;		
		double s = 1;		
		double xn = 0;		
		double xp = 0;
		
		do {			
			n++;			
			xp = xn;			
			f = f * (-2);			
			s = s * 3;
			xn = (f + s) / (f * (-2) + s * 3);			
		}
		while (Math.abs(xn - xp) > eps);		
		System.out.println(Math.abs(xn - xp));
	
	}
}	