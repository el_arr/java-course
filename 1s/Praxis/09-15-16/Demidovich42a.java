public class Demidovich42a{
	public static void main(String [] args) {
		
		double eps = Double.parseDouble(args[0]);
		double s = -1;
		double xp = 0;
		double xn = 1;
		double n = 1;
		double s1 = 1;
		
		do {
			n++;
			xp = xn;
			s1 = s1*s;
			xn = s1 / n;
		}
		while (Math.abs(xn - xp) > eps);
		System.out.println(Math.abs(xn - xp));
		
	}
}