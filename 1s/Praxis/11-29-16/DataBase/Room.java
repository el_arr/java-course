public class Room {

    private String roomID;
    private String roomName;
    private short beds;
    private String bedType;
    private short maxOccupancy;
    private short basePrice;
    private String decor;

    public Room(String rm) {
        String[] parts = rm.split(",");
        setRoomID(parts[0]);
        setRoomName(parts[1]);
        setBeds(new Short(parts[2]));
        setBedType(parts[3]);
        setMaxOccupancy(new Short(parts[4]));
        setBasePrice(new Short(parts[5]));
        setDecor(parts[6]);
    }

    public String getRoomID() {
        return roomID;
    }

    public void setRoomID(String roomID) {
        this.roomID = roomID;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public short getBeds() {
        return beds;
    }

    public void setBeds(short beds) {
        this.beds = beds;
    }

    public String getBedType() {
        return bedType;
    }

    public void setBedType(String bedType) {
        this.bedType = bedType;
    }

    public short getMaxOccupancy() {
        return maxOccupancy;
    }

    public void setMaxOccupancy(short maxOccupancy) {
        this.maxOccupancy = maxOccupancy;
    }

    public short getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(short basePrice) {
        this.basePrice = basePrice;
    }

    public String getDecor() {
        return decor;
    }

    public void setDecor(String decor) {
        this.decor = decor;
    }

    public boolean priceEquals(Room r) {
        if (this.basePrice == r.basePrice) {
            return true;
        }
        return false;
    }

    public boolean priceLower(Room r) {
        if (this.basePrice < r.basePrice) {
            return true;
        }
        return false;
    }

    public boolean priceHigher(Room r) {
        if (this.basePrice > r.basePrice) {
            return true;
        }
        return false;
    }

}
