public class Reservation {

    private String code;
    private Room room;
    private String checkIn;
    private String checkOut;
    private double rate;
    private String lastName;
    private String firstName;
    private short adults;
    private short kids;

    public Reservation() {

    }

    public Reservation(String rsv, Room[] rms) {
        String[] parts = rsv.split(",");
        setCode(parts[0]);
        for (Room r : rms) {
            if (parts[1].equals(r.getRoomID())) {
                setRoom(r);
            }
        }
        String temp2 = parts[2];
        if (temp2.matches("[/d/d]" + "[-]" + "[A-Z][A-Z][A-Z]" + "[-]" + "[/d/d]")) {
            setCheckIn(temp2);
        }
        String temp3 = parts[3];
        if (temp3.matches("[/d/d]" + "[-]" + "[A-Z][A-Z][A-Z]" + "[-]" + "[/d/d]")) {
            setCheckOut(temp3);
        }
        setRate(new Double(parts[4]));
        setLastName(parts[5]);
        setFirstName(parts[6]);
        setAdults(new Short(parts[7]));
        setKids(new Short(parts[8]));
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public String getCheckIn() {
        return checkIn;
    }

    public void setCheckIn(String checkIn) {
        this.checkIn = checkIn;
    }

    public String getCheckOut() {
        return checkOut;
    }

    public void setCheckOut(String checkOut) {
        this.checkOut = checkOut;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public short getAdults() {
        return adults;
    }

    public void setAdults(short adults) {
        this.adults = adults;
    }

    public short getKids() {
        return kids;
    }

    public void setKids(short kids) {
        this.kids = kids;
    }
}