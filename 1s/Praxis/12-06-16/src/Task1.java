import java.util.Scanner;

public class Task1 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        boolean not_found = true;
        int temp;
        int digit;
        System.out.println("Введите кол-во чисел (n)");
        int n = in.nextInt();
        System.out.println("Введите n чисел");
        for (int i = 0; i < n && not_found; i++) {
            temp = in.nextInt();
            while (temp > 0) {
                digit = temp % 10;
                if (digit % 2 == 1) {
                    not_found = true;
                    break;
                }
                temp /= 10;
                not_found = false;
            }
        }
        if (not_found) {
            System.out.println("Такое число, в котором все цифры чётные, не существует");
        } else {
            System.out.println("Существует такое число, в котором все цифры чётные");
        }
    }
}
