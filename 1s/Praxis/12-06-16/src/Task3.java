import java.util.Scanner;

public class Task3 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        boolean lengthEqualsK = true;
        int temp;
        int length = 0;
        System.out.println("Введите кол-во чисел (n)");
        int n = in.nextInt();
        System.out.println("Введите кол-во цифр (k)");
        int k = in.nextInt();
        System.out.println("Введите n чисел");
        for (int i = 0; (i < n) && lengthEqualsK; i++) {
            temp = in.nextInt();
            while (temp > 0) {
                length++;
                temp /= 10;
            }
            if (length != k) {
                lengthEqualsK = false;
            }
            length = 0;
        }
        if (!lengthEqualsK) {
            System.out.println("Не все числа содержат ровно k цифр");
        } else {
            System.out.println("Каждое введённое число содержит ровно k цифр");
        }
    }
}
