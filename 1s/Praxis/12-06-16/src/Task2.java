import java.util.Scanner;

public class Task2 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        boolean not_found = true;
        boolean digit_found = false;
        int temp;
        int digit;
        System.out.println("Введите кол-во чисел (n)");
        int n = in.nextInt();
        System.out.println("Введите n чисел");
        for (int i = 0; (i < n) && not_found; i++) {
            temp = in.nextInt();
            while (temp > 0 && !digit_found) {
                digit = temp % 10;
                temp /= 10;
                if (digit % 3 != 0) {
                    not_found = true;
                    continue;
                }
                not_found = false;
            }
        }
        if (not_found) {
            System.out.println("Есть число, в котором ни одна цифра не делится на 3");
        } else {
            System.out.println("Во всех числах есть цифра, которая делится на 3");
        }
    }
}
