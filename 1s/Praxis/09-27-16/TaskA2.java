public class TaskA2{

	public static void main(String [] args) {
	
		int n = args.length;
		int i = 0;
		int ct = 0;
		int c;
		
		while ((ct <= 2) && (i < n)){
			c = Integer.parseInt(args[i]);
			i++;
			if (c > 0){
				ct++;
			}
		}
		if (ct <= 2){
			System.out.println("+");
		}
		else{
			System.out.println("-");
		}
	
	}
	
}