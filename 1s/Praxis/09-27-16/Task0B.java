public class Task0B{

	public static void main(String [] args) {
	
		int n = args.length;
		int i = 0;
		double c;
		boolean fl = false;
		
		while ((i < n) && !fl) {
			c = Double.parseDouble(args[i]);
			i++;
			if ((c % 3 == 0) || (c % 5 == 0)){
				fl = true;
			}
		}
		if (fl){
			System.out.println("+");
		}
		else{
			System.out.println("-");
		}

	}

}