public class TaskA3{

	public static void main(String [] args) {
	
		int n = args.length;
		int i = 0;
		int cn = 0;
		int c;
		boolean fl = true;
		
		while ((fl) && (i < n)){
			c = Integer.parseInt(args[i]);
			while ((fl) && (c != 0)){
				cn = c % 10;
				if (cn % 2 == 1){
					fl = false;
				}
				c = c / 10;
			}
			i++;
		}
		if (fl){
			System.out.println("Все с четными цифрами");
		}
		else{
			System.out.println("Не все с четными цифрами");
		}
	
	}
	
}