import java.util.Scanner;

public class MatrixMain {

	public static void main(String [] args){
	
		//Scanner in = new Scanner(System.in);
		//int n = in.nextInt();
		Matrix m1 = new Matrix(new double[][]{
			{1,2},
			{3,4}
		});
		Matrix m2 = new Matrix(new double[][]{
			{1,0},
			{0,1}
		});
		System.out.println(m1);
		Matrix m3 = m1.mult(m2);
		System.out.println(m3);
		Matrix m4 = m1.getInverse();
		System.out.println("Inversed matrix for m1: ");
		System.out.println(m4);
		System.out.println("Check: ");
		System.out.println(m1.mult(m4));
	}
	
}