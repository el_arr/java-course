public class Matrix {

	private double [][] matrix = new double[2][2];
		
	public Matrix (double [][] matrix){
		this.matrix = matrix;
	}
	
	public Matrix (double a11, double a12, double a21, double a22){
		matrix[0][0] = a11;
		matrix[0][1] = a12;
		matrix[1][0] = a21;
		matrix[1][1] = a22;		
	}

	public Matrix (){
		this(0,0,0,0);
	}
	
	public String toString(){
		String str = "";
		for (int i = 0; i < matrix.length; i++){
			for (int j = 0; j < matrix[i].length; j++){
				str += matrix[i][j] + " ";
			}
			str += "\n";
		}
		return str;
	}
	
	public double getDeterminant() {
		return matrix[0][0]*matrix[1][1] - matrix[0][1]*matrix[1][0];
	}
	
	public Matrix getInverse (){
		Matrix result = new Matrix();
		double det = getDeterminant();
		result.matrix[0][0] = matrix[1][1]/det;
		result.matrix[0][1] = -1 * matrix[0][1]/det;
		result.matrix[1][0] = -1 * matrix[1][0]/det;
		result.matrix[1][1] = matrix[0][0]/det;
		return result;
	}
		
	public Matrix mult(Matrix m){
		Matrix result = new Matrix();
		double c;
		for (int i = 0; i < matrix.length; i++){
			for (int j = 0; j < matrix[i].length; j++){
				c = 0;
				for (int k = 0; k < matrix[i].length; k++){
					c += matrix[i][k]*m.matrix[k][j];
				}
				result.matrix[i][j] = c;
			}
		}
		return result;
	}

}