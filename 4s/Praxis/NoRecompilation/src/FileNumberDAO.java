import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class FileNumberDAO implements NumberDAO {
    private final String FILENAME = "lib/numbers.dat";

    public int getAnyNumber() {
        try {
            BufferedReader br = new BufferedReader(
                    new InputStreamReader(
                            new FileInputStream(FILENAME)
                    )
            );
            return Integer.valueOf(
                    br.readLine()
            );
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }

}
