import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    private static final String FILENAME = "lib/cfg.txt";

    public static void main(String[] args) {
        try {
            BufferedReader br = new BufferedReader(
                    new InputStreamReader(
                            new FileInputStream(FILENAME)));
            NumberDAO numberDAO = (NumberDAO) Class.forName(br.readLine()).newInstance();
            System.out.println(numberDAO.getAnyNumber());
        } catch (IllegalAccessException | IOException | InstantiationException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

}
