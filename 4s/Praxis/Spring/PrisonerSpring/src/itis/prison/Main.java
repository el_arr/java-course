package itis.prison;

import itis.prison.services.PrisonerService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {

    public static void main(String[] args) {
        ApplicationContext ac =
                new AnnotationConfigApplicationContext(Config.class);
        /*
        PrisonerDAO pd = ac.getBean(PrisonerDAO.class);
        System.out.println(pd.getRandomPrisonerName());
        */
        PrisonerService ps = ac.getBean(PrisonerService.class);
        ps.printRandomPrisoner();
    }

}
