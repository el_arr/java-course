package itis.prison.dao.impl;

import itis.prison.dao.PrisonerDAO;
import org.springframework.stereotype.Component;

@Component
public class SimplePrisonerDAO implements PrisonerDAO {

    @Override
    public String getRandomPrisonerName() {
        if (Math.random() > 0.5) {
            return "Kek";
        } else {
            return "Lol";
        }
    }

}
