package itis.prison;


import itis.prison.dao.PrisonerDAO;
import itis.prison.dao.impl.SimplePrisonerDAO;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"itis.prison.dao.impl", "itis.prison.services.impl"})
public class Config {
    /*
    @Bean
    public PrisonerDAO getPrisonerDAO() {
        return new SimplePrisonerDAO();
    }
    */

}
