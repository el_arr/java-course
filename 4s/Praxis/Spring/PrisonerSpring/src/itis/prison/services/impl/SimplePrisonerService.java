package itis.prison.services.impl;

import itis.prison.dao.PrisonerDAO;
import itis.prison.services.PrisonerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SimplePrisonerService implements PrisonerService{
    private PrisonerDAO prisonerDAO;

    @Autowired
    public SimplePrisonerService(PrisonerDAO prisonerDAO) {
        this.prisonerDAO = prisonerDAO;
    }

    public void printRandomPrisoner() {
        System.out.println("Random prisoner is: " + prisonerDAO.getRandomPrisonerName());
    }
}
