package com.mySampleApplication.server;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.mySampleApplication.client.MySampleApplicationService;

public class MySampleApplicationServiceImpl extends RemoteServiceServlet implements MySampleApplicationService {
    // Implementation of sample interface method
    public static String calculate(double first, String action, double second) {
        String result;
        switch (action) {
            case "+":
                result = first + second + "";
                break;
            case "-":
                result = first - second + "";
                break;
            case "*":
                result = first * second + "";
                break;
            case "/":
                result = first / second + "";
                break;
            default:
                result = "САНЬ, ХУЙ СОСИ";
        }
        return result;
    }
}