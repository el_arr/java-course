package com.mySampleApplication.client;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface MySampleApplicationServiceAsync {
    void calculate(double first, String action, double second, AsyncCallback<String> async);
}
