package ru.kpfu.itis.el_arr.exam.task5;

import ru.kpfu.itis.el_arr.exam.task5.client.Client;
import ru.kpfu.itis.el_arr.exam.task5.server.Server;
import ru.kpfu.itis.el_arr.exam.task5.util.Constants;

import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

import static ru.kpfu.itis.el_arr.exam.task5.util.Constants.HOST;
import static ru.kpfu.itis.el_arr.exam.task5.util.Constants.PORT;

public class App {
    private static final Scanner IN = new Scanner(System.in);
    private static Server server;

    public static void main(String[] args) {
        play();
    }

    private static void play() {
        Client client = null;
        try {
            client = new Client(new Socket(HOST, PORT));
        } catch (IOException e) {
            System.out.println(Constants.Interactions.WAITING_SERVER);
        }
        if (client != null) {
            boolean playing = (boolean) client.get();
            String answer = IN.next();
        } else {
            System.out.println(Constants.Interactions.ERROR_CLIENT);
        }
    }

}
