package ru.kpfu.itis.el_arr.exam.task5.client;

import ru.kpfu.itis.el_arr.exam.task5.util.ObjectExchanger;

import java.net.Socket;

public class Client {
    private Socket socket;

    public Client(Socket socket) {
        this.socket = socket;
    }

    public Object get() {
        return ObjectExchanger.get(socket);
    }

    public void send(String string) {
        ObjectExchanger.send(socket, string);
    }

    public Socket getSocket() {
        return socket;
    }

}
