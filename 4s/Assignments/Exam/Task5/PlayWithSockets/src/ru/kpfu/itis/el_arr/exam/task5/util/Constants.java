package ru.kpfu.itis.el_arr.exam.task5.util;

public class Constants {
    public static final int MIN_PLAYER_NUM = 2;
    public static final int PORT = 8080;
    public static final String HOST = "localhost";

    public class Interactions {
        public static final String PLAYER_GREETING = "Connection established. Greetings players. You may play now.";
        public static final String PLAYER_ENTER_ANSWER = "Enter city:";
        public static final String PLAYER_INCORRECT_ANSWER = "Answer is incorrect! Try again.";
        public static final String PLAYER_QUIT = "Q";
        public static final String WAITING_SERVER = "Server created. Waiting for players to connect.";
        public static final String WAITING_PLAYER = "Connection established. Waiting for players to connect.";
        public static final String ERROR_SERVER = "Server has not been not created. Please, restart the application to proceed.";
        public static final String ERROR_CLIENT = "Connection has not been established. Please, restart the application to proceed.";
    }
}
