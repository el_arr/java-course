/* Author: Eldar Mingachev */

package ru.kpfu.itis.el_arr.exam.task21;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list = generate();
        print(list, 0);
        list = sort(list);
        print(list, 1);
    }

    // Test list
    private static List<String> generate() {
        List<String> list = new ArrayList<>();
        list.add("123");
        list.add("1234");
        list.add("12");
        list.add("123456");
        list.add("1");
        return new ArrayList<>(list);
    }

    // Sort using comparator
    private static List<String> sort(List<String> input) {
        List<String> list = new ArrayList<>(input);
        // For given two strings a and b, sub[i][j] is the length of the common substring ending at first[i] and second[j].
        list.sort((first, second) -> {
            if (longestIncreasingSubstring(first) < longestIncreasingSubstring(second)) {
                return -1;
            } else if (longestIncreasingSubstring(first) > longestIncreasingSubstring(second)) {
                return 1;
            }
            return 0;
        });
        return new ArrayList<>(list);
    }

    // DP LIS algorithm
    private static int longestIncreasingSubstring(String string) {
        List<Character> longest = new ArrayList<>();
        List<Character> current = new ArrayList<>();
        int previous = Character.MAX_VALUE;
        char ch;
        for (int i = 0; i < string.length(); i++) {
            ch = string.charAt(i);
            if (ch <= previous) {
                if (longest.size() < current.size()) {
                    longest = current;
                }
                current = new ArrayList<>();
            }
            current.add(ch);
            previous = ch;
        }
        return longest.size() < current.size() ? current.size() : longest.size();
    }

    // Print list
    private static void print(List<String> list, int flag) {
        if (flag == 0) {
            System.out.println("Generated list:");
        } else {
            System.out.println("Sorted list:");
        }
        for (String s : list) {
            System.out.println(s);
        }
    }
}