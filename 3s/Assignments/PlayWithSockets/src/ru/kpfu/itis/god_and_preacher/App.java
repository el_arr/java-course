package ru.kpfu.itis.god_and_preacher;

import ru.kpfu.itis.god_and_preacher.client.Client;
import ru.kpfu.itis.god_and_preacher.model.Table;
import ru.kpfu.itis.god_and_preacher.model.player.entities.Player;
import ru.kpfu.itis.god_and_preacher.model.player.entities.PlayerGod;
import ru.kpfu.itis.god_and_preacher.model.player.entities.PlayerOrdinary;
import ru.kpfu.itis.god_and_preacher.server.Server;
import ru.kpfu.itis.god_and_preacher.util.Constants;

import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static ru.kpfu.itis.god_and_preacher.util.Constants.HOST;
import static ru.kpfu.itis.god_and_preacher.util.Constants.PORT;

public class App {
    private static final Scanner IN = new Scanner(System.in);
    private static Server server;
    private static Table table;
    private static Player player;

    public static void main(String[] args) {
        System.out.println(Constants.Interactions.ENTER_USERNAME);
        String username = IN.next();
        System.out.println(Constants.Interactions.USER_CHOICE);
        String systemQAnswer = IN.next();
        Pattern pattern = Pattern.compile(
                systemQAnswer.toLowerCase() + ".*"
        );
        Matcher create = pattern.matcher("create");
        if (create.find()) {
            System.out.println(Constants.Interactions.WAITING_SERVER);
            server = new Server();
            server.start();
        } else {
            playerMode(username);
        }
    }

    private static void playerMode(String username) {
        System.out.println(Constants.Interactions.GREETING_PLAYER);
        Client client = null;
        try {
            client = new Client(new Socket(HOST, PORT));
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (client != null) {
            client.setPlayer(player);
            System.out.println(Constants.Interactions.WAITING_PLAYER);
            //waiting for players to connect
            while (server.isNotFull()) {
                //do nothing
            }
            client.start();
        } else {
            System.out.println(Constants.Interactions.ERROR_CLIENT_PLAYER);
        }
    }





    private static void godMode(String username) {
        System.out.println(Constants.Interactions.GREETING_GOD);
        player = new PlayerGod(username, IN.next());
        while (server.isNotFull()) {
            //do nothing
        }
        List<Player> players = new ArrayList<>();
        for (Client client : Server.getClients()) {
            players.add(client.getPlayer());
        }
        table = new Table(players);
        player.setTable(table);
        Client client = null;
        try {
            Socket socket = new Socket(HOST, PORT);
            for (Client c : Server.getClients()) {
                if (c.getSocket().equals(socket)) {
                    client = c;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (client != null) {
            client.setPlayer(player);
            client.start();
        } else {
            System.out.println(Constants.Interactions.ERROR_CLIENT_GOD);
        }
    }



}
