package ru.kpfu.itis.god_and_preacher.model.player.entities;

import ru.kpfu.itis.god_and_preacher.model.Hand;
import ru.kpfu.itis.god_and_preacher.model.player.behaviour.PlayerOrdinaryBehaviour;
import ru.kpfu.itis.god_and_preacher.server.Server;

public class PlayerOrdinary extends Player {
    private Hand hand;

    public PlayerOrdinary(String username) {
        super(username);
        this.hand = new Hand();
        this.playerBehaviour = new PlayerOrdinaryBehaviour(this);
    }

    public Hand getHand() {
        return hand;
    }

    public void offerCard(int index) {
        table.offerCard(
                this.hand.playCard(index)
        );
        Server.notifyAllClients(username + " offers the card " + hand.getCard(index));
    }

}
