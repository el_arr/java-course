package ru.kpfu.itis.god_and_preacher.server;

import ru.kpfu.itis.god_and_preacher.client.Client;
import ru.kpfu.itis.god_and_preacher.model.Table;
import ru.kpfu.itis.god_and_preacher.model.player.entities.Player;
import ru.kpfu.itis.god_and_preacher.model.player.entities.PlayerGod;
import ru.kpfu.itis.god_and_preacher.model.player.entities.PlayerOrdinary;
import ru.kpfu.itis.god_and_preacher.util.ObjectExchanger;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.List;

import static ru.kpfu.itis.god_and_preacher.util.Constants.MIN_PLAYER_NUM;
import static ru.kpfu.itis.god_and_preacher.util.Constants.PORT;

public class Server {
    private static List<Client> clients;
    private boolean full = false;
    private Table table;

    public Server() {
        clients = new ArrayList<>();
    }

    public void start() {
        try {
            ServerSocket serverSocket = new ServerSocket(PORT);
            Client client =
                    new Client(
                            serverSocket.accept(),
                            new PlayerGod("", "")
                    );
            ObjectExchanger.send(client.getSocket(), client.getPlayer());
            clients.add(client);
            while (clients.size() <= MIN_PLAYER_NUM) {
                client =
                        new Client(
                                serverSocket.accept(),
                                new PlayerOrdinary("")
                        );
                ObjectExchanger.send(client.getSocket(), client.getPlayer());
                clients.add(client);
            }
            full = true;
            feedback();
            game();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void feedback() {
        for (Client client: clients) {
            if (c lient.getPlayer().getId() == 0) {
                client.setPlayer(
                        (PlayerGod) ObjectExchanger.get(client.getSocket())
                );
            }

        }
    }

    private void game() {
        Client current;
        for (Client client : clients) {
            ObjectExchanger.send(client.getSocket(), client.getPlayer());
        }
        table = (Table) ObjectExchanger.get(clients.get(0).getSocket());
        while (full) {
            for (int i = 1; i < clients.size(); i++) {
                current = clients.get(i);
                ObjectExchanger.send(current.getSocket(), table);
                Object object = ObjectExchanger.get(current.getSocket());
                if (object instanceof Table) {
                    table = (Table) object;
                }
                notifyAllClients(object);
            }
        }
    }

    public static void notifyAllClients(Object o) {
        for (Client client : clients) {
            ObjectExchanger.send(client.getSocket(), o);
        }
    }

    public static List<Client> getClients() {
        return clients;
    }

    public boolean isNotFull() {
        return !full;
    }

}
