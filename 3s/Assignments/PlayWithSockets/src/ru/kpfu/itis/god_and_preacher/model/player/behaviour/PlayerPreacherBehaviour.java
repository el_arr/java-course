package ru.kpfu.itis.god_and_preacher.model.player.behaviour;

import ru.kpfu.itis.god_and_preacher.model.Table;
import ru.kpfu.itis.god_and_preacher.model.player.entities.PlayerPreacher;

public class PlayerPreacherBehaviour implements PlayerBehaviour {
    private PlayerPreacher player;

    public PlayerPreacherBehaviour(PlayerPreacher player) {
        this.player = player;
    }

    public Table behave(Table table) {
        return table;
    }

}
