package ru.kpfu.itis.god_and_preacher.util;

public class Constants {
    public static final int MIN_PLAYER_NUM = 4;
    public static final int PORT = 8080;
    public static final String HOST = "localhost";

    public class Interactions {
        public static final String ENTER_USERNAME = "Enter username:";
        public static final String USER_CHOICE = "Join or create a new game?";
        public static final String GREETING_GOD = "Hail, our god!\n" +
                "Now you should create the 'World Rule', according to which you'll define which card is right, which - is not.\n" +
                "Once somebody reveals it - he can become the Preacher and tell the truth from your name. Starting from the next card\n" +
                "If the prophecy is false - you can 'PUNISH'(type it) the prophet and make him mortal again\n" +
                "Or if the next 10 prophecies are true, the prophet should tell everybody the 'World Rule'\n" +
                "If he is right - he wins the game, else - game continues until you'll run out of cards.\n\n" +
                "For now - enter the 'World Rule':";
        public static final String GREETING_PLAYER = "You are the player now.\n" +
                "Use your cards to reveal the 'World Rule', become the Preacher and win!\n" +
                "If you guess the rule - you can become the Preacher and tell, which card is right, which - is not. Just type 'PREACHER'\n" +
                "But be careful: once you make a mistake - you'll get punished\n" +
                "If the next 10 prophecies are true, you'll have to tell everybody the 'World Rule'\n" +
                "If you're right - you'll win the game, else - game continues until you'll run out of cards.";
        public static final String WAITING_SERVER = "Server created. Waiting for players to connect";
        public static final String WAITING_PLAYER = "Connection established. Waiting for other players to connect";
        public static final String ERROR_CLIENT_GOD = "Server is not created. Please, restart the application to proceed";
        public static final String ERROR_CLIENT_PLAYER = "Connection is not established. Please, restart the application to proceed";
    }
}
