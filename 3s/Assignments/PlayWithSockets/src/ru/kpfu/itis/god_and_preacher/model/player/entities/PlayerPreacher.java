package ru.kpfu.itis.god_and_preacher.model.player.entities;

import ru.kpfu.itis.god_and_preacher.model.player.behaviour.PlayerPreacherBehaviour;
import ru.kpfu.itis.god_and_preacher.server.Server;

public class PlayerPreacher extends Player {
    private PlayerOrdinary playerOrdinary;

    PlayerPreacher(PlayerOrdinary playerOrdinary) {
        this.playerOrdinary = playerOrdinary;
        this.playerBehaviour = new PlayerPreacherBehaviour(this);
        Server.notifyAllClients("I'm the Preacher now!");
    }

    public PlayerOrdinary punished() {
        return playerOrdinary;
    }

}
