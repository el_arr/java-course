package ru.kpfu.itis.god_and_preacher.model.player.entities;

import ru.kpfu.itis.god_and_preacher.model.Table;
import ru.kpfu.itis.god_and_preacher.model.player.behaviour.PlayerBehaviour;

import java.io.Serializable;

public class Player implements Serializable {
    private static int counter = 0;
    private int id;
    boolean god = false;
    PlayerBehaviour playerBehaviour;
    String username;
    Table table;

    Player() {
        this.id = counter++;
    }

    Player(String username) {
        this();
        this.username = username;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setTable(Table table) {
        this.table = table;
    }

    public Table getTable() {
        return table;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public PlayerBehaviour getPlayerBehaviour() {
        return playerBehaviour;
    }
}
