package ru.kpfu.itis.god_and_preacher.data.service;

import ru.kpfu.itis.god_and_preacher.data.repository.CardRepositoryImpl;
import ru.kpfu.itis.god_and_preacher.data.repository.Repository;
import ru.kpfu.itis.god_and_preacher.model.Card;

import java.util.List;

public class CardService {
    private static Repository<Card> cardRepository = new CardRepositoryImpl();

    public static List<Card> getAll() {
        return cardRepository.getByQuery(
                "SELECT suit.name as suit, type.name as type FROM suit CROSS JOIN type"
        );
    }

}
