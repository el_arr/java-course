package ru.kpfu.itis.god_and_preacher.model.player.behaviour;

import ru.kpfu.itis.god_and_preacher.model.Table;
import ru.kpfu.itis.god_and_preacher.model.player.entities.PlayerGod;
import ru.kpfu.itis.god_and_preacher.model.player.entities.PlayerOrdinary;
import ru.kpfu.itis.god_and_preacher.model.player.entities.PlayerPreacher;
import ru.kpfu.itis.god_and_preacher.server.Server;

public class PlayerGodBehaviour implements PlayerBehaviour {
    private PlayerGod player;

    public PlayerGodBehaviour(PlayerGod player) {
        this.player = player;
    }

    public Table behave(Table table) {
        return table;
    }

    public PlayerOrdinary punishPreacher(PlayerPreacher preacher) {
        Server.notifyAllClients("God " + player.getUsername() + " punishes the fake preacher " + preacher.getUsername() + "!\n");
        return preacher.punished();
    }

    public boolean godDecides(String s) {
        boolean result = s.toLowerCase().equals("y") || s.toLowerCase().equals("yes");
        if (result) {
            Server.notifyAllClients("God says the card is right.\n");
        } else {
            Server.notifyAllClients("God says the card is not right.");
        }
        return result;
    }

}
