package ru.kpfu.itis.god_and_preacher.model.player.behaviour;

import ru.kpfu.itis.god_and_preacher.model.Table;
import ru.kpfu.itis.god_and_preacher.model.player.entities.PlayerOrdinary;

public class PlayerOrdinaryBehaviour implements PlayerBehaviour {
    private PlayerOrdinary player;

    public PlayerOrdinaryBehaviour(PlayerOrdinary player) {
        this.player = player;
    }

    public Table behave(Table table) {
        return table;
    }

}
