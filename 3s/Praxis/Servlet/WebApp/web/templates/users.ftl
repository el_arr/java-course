<html>

<head>
    <title>WebApp: Users</title>
    <link href="css/style.css" rel="stylesheet">
    <script type="application/javascript" src="lib/js/jquery-3.2.1.js"></script>
</head>

<body>
<div class="search">
    <input type="text" name="q" id="q_id" oninput="f()"/>

    <ul id="results">
</div>
<script type="application/javascript">
    function f() {
        console.log(1);
        $.ajax({
            url: "/user-search",
            "data": {"q": $("#q_id").val()},
            "dataType": "json",
            success: function (result) {
                $("#results").html("");
                for (var i = 0; i < result.ids.length; i++) {
                    var href = "<a href='/user/" + result.ids[i] + "'>";
                    $("#results").append("<li>" + href + result.names[i] + "</a></li>");
                }
                $("#results").append("</ul>")
            },
            error: function () {
                console.log("OOOOps");
            }
        });
    }
</script>

<div>
    <h1>User list</h1>
<#list users as user>
    <li>
        <h4>${user.getName()}</h4>
        <ul>
            <form action="/users" method="post">
                <input type="submit" name="edit_id=${user.getId()}" value="Edit"/>
                <input type="submit" name="delete_id=${user.getId()}" value="Delete"/>
            </form>
        </ul>
    </li>
</#list>
</div>
</body>

</html>