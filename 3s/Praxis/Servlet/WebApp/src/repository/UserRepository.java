package repository;

import entity.User;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserRepository {
    private static java.sql.Connection conn = cfg.database.Connection.getConnection();

    /*
     * резонно ли обращаться каждый раз к бд, через метод select, каждый раз уточняя запрос?
     * или же лучше взять, например, селект по первой букве, загрузить из бд, а затем уже его уточнять внутри программы?
     */
    public static List<User> search(String q) {
        if (!"".equals(q)) {
            try {
                List<User> list = new ArrayList<>();
                PreparedStatement st = conn.prepareStatement(
                        "SELECT users.id, users.name FROM users "
                );
                ResultSet rs = st.executeQuery();
                if (rs != null) {
                    while (rs.next()) {
                        String s = rs.getString("name");
                        Pattern p = Pattern.compile(
                                "(.*" + q + ".*)"
                        );
                        Matcher mU = p.matcher(s.toUpperCase());
                        Matcher m = p.matcher(s);
                        Matcher mL = p.matcher(s.toLowerCase());
                        if (mU.find() || m.find() || mL.find()) {
                            list.add(new User(rs.getInt("id"), s));
                        }
                    }
                    return list;
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static List<Object> getAllUsers() {
        List<Object> users = new ArrayList<>();
        try {
            PreparedStatement st = conn.prepareStatement(
                    "SELECT users.id, users.name, users.age, groups.name AS group_name FROM users " +
                            "JOIN groups ON users.group_id = groups.id"
            );
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                users.add(
                        new User(
                                rs.getInt("id"),
                                rs.getString("name"),
                                rs.getInt("age"),
                                rs.getString("group_name")
                        )
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return users;
    }

    public static User getUser(int id) {
        try {
            PreparedStatement st = conn.prepareStatement(
                    "SELECT users.id, users.name, users.age, groups.name AS group_name FROM users " +
                            "JOIN groups ON users.group_id = groups.id WHERE users.id = ?"
            );
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return new User(
                        rs.getInt("id"),
                        rs.getString("name"),
                        rs.getInt("age"),
                        rs.getString("group_name")
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void deleteUser(int id) {
        try {
            PreparedStatement st1 = conn.prepareStatement(
                    "DELETE FROM auth WHERE auth.id = ?;"
            );
            PreparedStatement st2 = conn.prepareStatement(
                    "DELETE FROM users WHERE users.id = ?;"
            );
            st1.setInt(1, id);
            st2.setInt(1, id);
            st1.executeUpdate();
            st2.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}