package repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class LoginRepository {
    private static java.sql.Connection conn = cfg.database.Connection.getConnection();

    public static boolean check(String username, String password) {
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(
                    "SELECT username FROM auth WHERE password = '" + password + "'"
            );
            if (rs.next()) {
                return rs.getString("username")
                        .equals(username);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
}