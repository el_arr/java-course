package servlet;

import cfg.view.Freemarker;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import repository.LoginRepository;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class LoginServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        if (LoginRepository.check(username, password)) {
            request.getSession().setAttribute("current_user", username);
            response.sendRedirect("/users");
        } else {
            response.sendRedirect("/login");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Template temp = Freemarker.getCfg().getTemplate("login.ftl");
        try {
            temp.process(null, response.getWriter());
        } catch (TemplateException e) {
            e.printStackTrace();
        }
    }

}