package servlet.ajax;

import entity.User;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import repository.UserRepository;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class UserSearchServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String q = request.getParameter("q");
        JSONObject jo = new JSONObject();
        JSONArray names = new JSONArray();
        JSONArray ids = new JSONArray();
        try {
            List<User> list = UserRepository.search(q);
            if (list != null) {
                for (User o : list) {
                    ids.put(o.getId());
                    names.put(o.getName());
                }
            }
            jo.put("names", names);
            jo.put("ids", ids);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        response.setContentType("text/json");
        response.getWriter().print(jo.toString());
        response.getWriter().close();
    }
}
