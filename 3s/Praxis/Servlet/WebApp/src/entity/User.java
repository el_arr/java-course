package entity;

public class User {
    private int id;
    private String name;
    private int age;
    private String group_name;

    public User(int id, String name, int age, String group_name) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.group_name = group_name;
    }

    public User(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public String getGroup() {
        return group_name;
    }

    @Override
    public String toString() {
        return name + ", " + age + ", " + group_name;
    }
}
