import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        try {
            Class cv = Class.forName(scanner.next());
            Class cv2 = Class.forName(scanner.next());
            String methodName = scanner.next();
            Method m = cv.getMethod(methodName, cv2);
            Object o1 = cv.newInstance();
            Object o2 = cv.newInstance();
            System.out.println(m.invoke(o1, o2));
        } catch (IllegalAccessException | InstantiationException | InvocationTargetException |
                NoSuchMethodException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

}
