public class HelloWorld {

    public String greet(String lang) {
        lang = lang.toLowerCase();
        switch (lang) {
            case "en":
                return "Hello";
            case "es":
                return "Hola";
            case "ru":
                return "Здарова, пидор";
            default:
                return "Null";
        }
    }

    public static void main(String[] args) {
        HelloWorld hw = new HelloWorld();
        System.out.println(hw.greet("ru"));
    }

}
